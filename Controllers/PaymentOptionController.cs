﻿#region Init
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CATS.Data;
using CATS.Models;
#endregion

namespace CATS.Controllers
{
	public class PaymentOptionController : Controller
	{
		#region Declarations
		private readonly CATSDatabaseContext oCATSDatabaseContext;

		public PaymentOptionController(CATSDatabaseContext context)
		{ oCATSDatabaseContext = context; }
		#endregion


			#region Routines
			private bool PaymentOptionExists(int pPaymentOptionID)
		{ return oCATSDatabaseContext.PaymentOption.Any(oPaymentOption => oPaymentOption.PaymentOptionID == pPaymentOptionID); }
		#endregion

		#region Index
		public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.DescriptionSortParm = sortOrder == "Description" ? "Description desc" : "Description";

			if (searchString != null)
			{
				page = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["currentFilter"] = searchString;

			var query = from oPaymentOption in oCATSDatabaseContext.PaymentOption
									where oPaymentOption.DeleteFlag == false && oPaymentOption.CompanyID == Globals.CompanyID
									select oPaymentOption;

			if (!String.IsNullOrEmpty(searchString))
			{
				query = query.Where(select => select.Description.Contains(searchString));
			}
			switch (sortOrder)
			{
				case "Description desc":
					query = query.OrderByDescending(select => select.Description);
					break;
				default:
					query = query.OrderBy(select => select.Description);
					break;
			}

			return View(await PaginatedList<PaymentOption>.CreateAsync(query.AsNoTracking(), page ?? 1, Globals.PageSize));
		}
		#endregion

		#region CreateEdit
		public IActionResult CreateEdit()
		{
			var oPaymentOption = new PaymentOption
			{
				PaymentOptionID = 0,
				CompanyID = Globals.CompanyID,
				DeleteFlag = false
				};
			return View(oPaymentOption);
	}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit([Bind("PaymentOptionID, CompanyID, Description, DeleteFlag, RecordUpdate")] PaymentOption oPaymentOption)
		{
			if (ModelState.IsValid)
			{
				oPaymentOption.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Add(oPaymentOption);
				await oCATSDatabaseContext.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(oPaymentOption);
		}

		[HttpGet("PaymentOption/CreateEdit/{pPaymentOptionID:int}")]
		public async Task<IActionResult> CreateEdit(int? pPaymentOptionID)
		{
			if (pPaymentOptionID == null) { return NotFound(); }
			var oPaymentOption = await oCATSDatabaseContext.PaymentOption.SingleOrDefaultAsync(qPaymentOption => qPaymentOption.PaymentOptionID == pPaymentOptionID);
			if (oPaymentOption == null) { return NotFound(); }
			return View(oPaymentOption);
		}

		[HttpPost("PaymentOption/CreateEdit/{pPaymentOptionID:int}")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit(int pPaymentOptionID, [Bind("PaymentOptionID, CompanyID, Description, DeleteFlag, RecordUpdate")] PaymentOption oPaymentOption)
		{
			if (ModelState.IsValid)
			{
				try
				{
					oPaymentOption.PaymentOptionID = pPaymentOptionID;
					oPaymentOption.RecordUpdate = DateTime.Now;
					oCATSDatabaseContext.Update(oPaymentOption);
					await oCATSDatabaseContext.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!PaymentOptionExists(oPaymentOption.PaymentOptionID))
					{ return NotFound(); }
					else
					{ throw; }
				}
				return RedirectToAction(nameof(Index));
			}
			return View(oPaymentOption);
		}
		#endregion

		#region Delete
		[HttpGet("PaymentOption/Delete/{pPaymentOptionID:int}")]
		public async Task<IActionResult> Delete(int? pPaymentOptionID)
		{
			if (pPaymentOptionID == null) { return NotFound(); }
			var oPaymentOption = await oCATSDatabaseContext.PaymentOption.SingleOrDefaultAsync(qPaymentOption => qPaymentOption.PaymentOptionID == pPaymentOptionID);
			if (oPaymentOption == null) { return NotFound(); }
			return View(oPaymentOption);
		}

		[HttpPost("PaymentOption/Delete/{pPaymentOptionID:int}"), ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int? pPaymentOptionID)
		{
			if (pPaymentOptionID == null) { return NotFound(); }
			var oPaymentOption = await oCATSDatabaseContext.PaymentOption.SingleOrDefaultAsync(qPaymentOption => qPaymentOption.PaymentOptionID == pPaymentOptionID);
			if (oPaymentOption == null) { return NotFound(); }
			try
			{
				oPaymentOption.DeleteFlag = true;
				oPaymentOption.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Update(oPaymentOption);
				await oCATSDatabaseContext.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!PaymentOptionExists(oPaymentOption.PaymentOptionID))
				{ return NotFound(); }
				else
				{ throw; }
			}
			return RedirectToAction(nameof(Index));
		}
		#endregion
	}
}
