﻿#region Init
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CATS.Data;
using CATS.Models;
#endregion

namespace CATS.Controllers
{
	public class EducationalInstitutionController : Controller
	{
		#region Declarations
		private readonly CATSDatabaseContext oCATSDatabaseContext;

		public EducationalInstitutionController(CATSDatabaseContext context)
		{ oCATSDatabaseContext = context; }
		#endregion

		#region Routines
		private bool EducationalInstitutionExists(int pEducationalInstitutionID)
		{ return oCATSDatabaseContext.EducationalInstitution.Any(oEducationalInstitution => oEducationalInstitution.EducationalInstitutionID == pEducationalInstitutionID); }
		#endregion

		#region Index
		public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.DescriptionSortParm = sortOrder == "Description" ? "Description desc" : "Description";

			if (searchString != null)
			{
				page = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["currentFilter"] = searchString;

			var query = from oEducationalInstitution in oCATSDatabaseContext.EducationalInstitution
									where oEducationalInstitution.DeleteFlag == false && oEducationalInstitution.CompanyID == Globals.CompanyID
									select oEducationalInstitution;

			if (!String.IsNullOrEmpty(searchString))
			{
				query = query.Where(select => select.Description.Contains(searchString));
			}
			switch (sortOrder)
			{
				case "Description desc":
					query = query.OrderByDescending(select => select.Description);
					break;
				default:
					query = query.OrderBy(select => select.Description);
					break;
			}

			return View(await PaginatedList<EducationalInstitution>.CreateAsync(query.AsNoTracking(), page ?? 1, Globals.PageSize));
		}
		#endregion

		#region CreateEdit
		public IActionResult CreateEdit()
		{
			var oEducationalInstitution = new EducationalInstitution
			{
				EducationalInstitutionID = 0,
				CompanyID = Globals.CompanyID,
				DeleteFlag = false
			};
			return View(oEducationalInstitution);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit([Bind("EducationalInstitutionID, CompanyID, Description, DeleteFlag, RecordUpdate")] EducationalInstitution oEducationalInstitution)
		{
			if (ModelState.IsValid)
			{
				oEducationalInstitution.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Add(oEducationalInstitution);
				await oCATSDatabaseContext.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(oEducationalInstitution);
		}

		[HttpGet("EducationalInstitution/CreateEdit/{pEducationalInstitutionID:int}")]
		public async Task<IActionResult> CreateEdit(int? pEducationalInstitutionID)
		{
			if (pEducationalInstitutionID == null) { return NotFound(); }
			var oEducationalInstitution = await oCATSDatabaseContext.EducationalInstitution.SingleOrDefaultAsync(qEducationalInstitution => qEducationalInstitution.EducationalInstitutionID == pEducationalInstitutionID);
			if (oEducationalInstitution == null) { return NotFound(); }
			return View(oEducationalInstitution);
		}

		[HttpPost("EducationalInstitution/CreateEdit/{pEducationalInstitutionID:int}")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit(int pEducationalInstitutionID, [Bind("EducationalInstitutionID, CompanyID, Description, DeleteFlag, RecordUpdate")] EducationalInstitution oEducationalInstitution)
		{
			if (ModelState.IsValid)
			{
				try
				{
					oEducationalInstitution.EducationalInstitutionID = pEducationalInstitutionID;
					oEducationalInstitution.RecordUpdate = DateTime.Now;
					oCATSDatabaseContext.Update(oEducationalInstitution);
					await oCATSDatabaseContext.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!EducationalInstitutionExists(oEducationalInstitution.EducationalInstitutionID))
					{ return NotFound(); }
					else
					{ throw; }
				}
				return RedirectToAction(nameof(Index));
			}
			return View(oEducationalInstitution);
		}
		#endregion

		#region Delete
		[HttpGet("EducationalInstitution/Delete/{pEducationalInstitutionID:int}")]
		public async Task<IActionResult> Delete(int? pEducationalInstitutionID)
		{
			if (pEducationalInstitutionID == null) { return NotFound(); }
			var oEducationalInstitution = await oCATSDatabaseContext.EducationalInstitution.SingleOrDefaultAsync(qEducationalInstitution => qEducationalInstitution.EducationalInstitutionID == pEducationalInstitutionID);
			if (oEducationalInstitution == null) { return NotFound(); }
			return View(oEducationalInstitution);
		}

		[HttpPost("EducationalInstitution/Delete/{pEducationalInstitutionID:int}"), ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int? pEducationalInstitutionID)
		{
			if (pEducationalInstitutionID == null) { return NotFound(); }
			var oEducationalInstitution = await oCATSDatabaseContext.EducationalInstitution.SingleOrDefaultAsync(qEducationalInstitution => qEducationalInstitution.EducationalInstitutionID == pEducationalInstitutionID);
			if (oEducationalInstitution == null) { return NotFound(); }
			try
			{
				oEducationalInstitution.DeleteFlag = true;
				oEducationalInstitution.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Update(oEducationalInstitution);
				await oCATSDatabaseContext.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!EducationalInstitutionExists(oEducationalInstitution.EducationalInstitutionID))
				{ return NotFound(); }
				else
				{ throw; }
			}
			return RedirectToAction(nameof(Index));
		}
		#endregion
	}
}
