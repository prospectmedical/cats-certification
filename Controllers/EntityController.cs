﻿#region Init
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CATS.Data;
using CATS.Models;
#endregion

namespace CATS.Controllers
{
	public class EntityController : Controller
	{
		#region Declarations
		private readonly CATSDatabaseContext oCATSDatabaseContext;

		public EntityController(CATSDatabaseContext context)
		{ oCATSDatabaseContext = context; }
		#endregion

		#region Routines
		private bool EntityExists(int pEntityID)
		{ return oCATSDatabaseContext.Entity.Any(oEntity => oEntity.EntityID == pEntityID); }
		#endregion

		#region Index
		public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.DescriptionSortParm = sortOrder == "Description" ? "Description desc" : "Description";

			if (searchString != null)
			{
				page = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["currentFilter"] = searchString;

			var query = from oEntity in oCATSDatabaseContext.Entity
									where oEntity.DeleteFlag == false && oEntity.CompanyID == Globals.CompanyID
									select oEntity;

			if (!String.IsNullOrEmpty(searchString))
			{
				query = query.Where(select => select.Description.Contains(searchString));
			}
			switch (sortOrder)
			{
				case "Description desc":
					query = query.OrderByDescending(select => select.Description);
					break;
				default:
					query = query.OrderBy(select => select.Description);
					break;
			}

			return View(await PaginatedList<Entity>.CreateAsync(query.AsNoTracking(), page ?? 1, Globals.PageSize));
		}
		#endregion

		#region CreateEdit
		public IActionResult CreateEdit()
		{
			var oEntity = new Entity
			{
				EntityID = 0,
				CompanyID = Globals.CompanyID,
				DeleteFlag = false
			};
			return View(oEntity);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit([Bind("EntityID, CompanyID, Description, DeleteFlag, RecordUpdate")] Entity oEntity)
		{
			if (ModelState.IsValid)
			{
				oEntity.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Add(oEntity);
				await oCATSDatabaseContext.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(oEntity);
		}

		[HttpGet("Entity/CreateEdit/{pEntityID:int}")]
		public async Task<IActionResult> CreateEdit(int? pEntityID)
		{
			if (pEntityID == null) { return NotFound(); }
			var oEntity = await oCATSDatabaseContext.Entity.SingleOrDefaultAsync(qEntity => qEntity.EntityID == pEntityID);
			if (oEntity == null) { return NotFound(); }
			return View(oEntity);
		}

		[HttpPost("Entity/CreateEdit/{pEntityID:int}")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit(int pEntityID, [Bind("EntityID, CompanyID, Description, DeleteFlag, RecordUpdate")] Entity oEntity)
		{
			if (ModelState.IsValid)
			{
				try
				{
					oEntity.EntityID = pEntityID;
					oEntity.RecordUpdate = DateTime.Now;
					oCATSDatabaseContext.Update(oEntity);
					await oCATSDatabaseContext.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!EntityExists(oEntity.EntityID))
					{ return NotFound(); }
					else
					{ throw; }
				}
				return RedirectToAction(nameof(Index));
			}
			return View(oEntity);
		}
		#endregion

		#region Delete
		[HttpGet("Entity/Delete/{pEntityID:int}")]
		public async Task<IActionResult> Delete(int? pEntityID)
		{
			if (pEntityID == null) { return NotFound(); }
			var oEntity = await oCATSDatabaseContext.Entity.SingleOrDefaultAsync(qEntity => qEntity.EntityID == pEntityID);
			if (oEntity == null) { return NotFound(); }
			return View(oEntity);
		}

		[HttpPost("Entity/Delete/{pEntityID:int}"), ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int? pEntityID)
		{
			if (pEntityID == null) { return NotFound(); }
			var oEntity = await oCATSDatabaseContext.Entity.SingleOrDefaultAsync(qEntity => qEntity.EntityID == pEntityID);
			if (oEntity == null) { return NotFound(); }
			try
			{
				oEntity.DeleteFlag = true;
				oEntity.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Update(oEntity);
				await oCATSDatabaseContext.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!EntityExists(oEntity.EntityID))
				{ return NotFound(); }
				else
				{ throw; }
			}
			return RedirectToAction(nameof(Index));
		}
		#endregion
	}
}
