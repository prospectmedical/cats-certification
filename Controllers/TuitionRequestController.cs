﻿#region Init
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CATS.Data;
using CATS.Models;
using System.Collections.Generic;
using System.Globalization;
#endregion

namespace CATS.Controllers
{
	public class TuitionRequestController : Controller
	{
		#region Declarations
		private CATSDatabaseContext oCATSDatabaseContext;
		//private UnityInterfacesDatabaseContext oUnityInterfacesDatabaseContext;
		private OracleHRDatabaseContext oOracleHRDatabaseContext;

		public TuitionRequestController(
					[FromServices] CATSDatabaseContext pCATSDatabaseContext,
					//[FromServices] UnityInterfacesDatabaseContext pUnityInterfacesDatabaseContext,
					[FromServices] OracleHRDatabaseContext pOracleHRDatabaseContext)
		{
			oCATSDatabaseContext = pCATSDatabaseContext;
			//oUnityInterfacesDatabaseContext = pUnityInterfacesDatabaseContext;
			oOracleHRDatabaseContext = pOracleHRDatabaseContext;
		}
		#endregion

		#region Routines
		private bool TuitionRequestExists(int pTuitionRequestID)
		{ return oCATSDatabaseContext.TuitionRequests.Any(oTuitionRequest => oTuitionRequest.TuitionRequestID == pTuitionRequestID); }

		private void GetPaymentOptionList()
		{
			List<PaymentOption> oPaymentOptionList = new List<PaymentOption>();
			oPaymentOptionList = (from oPaymentOption in oCATSDatabaseContext.PaymentOption
														where oPaymentOption.DeleteFlag == false && oPaymentOption.CompanyID == Globals.CompanyID
														orderby oPaymentOption.Description
														select oPaymentOption).ToList();
			//oPaymentOptionList.Insert(0, new PaymentOption { PaymentOptionID = 0, Description = "Select" });
			ViewBag.ListOfPaymentOption = oPaymentOptionList;
		}

		private void GetFinancialAidTypeList()
		{
			List<FinancialAidType> oFinancialAidTypeList = new List<FinancialAidType>();
			oFinancialAidTypeList = (from oFinancialAidType in oCATSDatabaseContext.FinancialAidType
															 where oFinancialAidType.DeleteFlag == false && oFinancialAidType.CompanyID == Globals.CompanyID
															 orderby oFinancialAidType.Description
														select oFinancialAidType).ToList();
			oFinancialAidTypeList.Insert(0, new FinancialAidType { FinancialAidTypeID = 0, Description = "Select" });
			ViewBag.ListOfFinancialAidType = oFinancialAidTypeList;
		}

		private void GetEducationalInstitutionList()
		{
			List<EducationalInstitution> oEducationalInstitutionList = new List<EducationalInstitution>();
			oEducationalInstitutionList = (from oEducationalInstitution in oCATSDatabaseContext.EducationalInstitution
																		 where oEducationalInstitution.DeleteFlag == false && oEducationalInstitution.CompanyID == Globals.CompanyID
																		 orderby oEducationalInstitution.Description
															 select oEducationalInstitution).ToList();
			oEducationalInstitutionList.Insert(0, new EducationalInstitution { EducationalInstitutionID = 0, Description = "Select" });
			ViewBag.ListOfEducationalInstitution = oEducationalInstitutionList;
		}

		private void GetEducationalProgramList()
		{
			List<EducationalProgram> oEducationalProgramList = new List<EducationalProgram>();
			oEducationalProgramList = (from oEducationalProgram in oCATSDatabaseContext.EducationalProgram
											where oEducationalProgram.DeleteFlag == false && oEducationalProgram.CompanyID == Globals.CompanyID
											orderby oEducationalProgram.SortOrder
															 select oEducationalProgram).ToList();
			oEducationalProgramList.Insert(0, new EducationalProgram { EducationalProgramID = 0, Description = "Select" });
			ViewBag.ListOfEducationalProgram = oEducationalProgramList;
		}

		private void GetMajorList()
		{
			List<Major> oMajorList = new List<Major>();
			oMajorList = (from oMajor in oCATSDatabaseContext.Major
										where oMajor.DeleteFlag == false && oMajor.CompanyID == Globals.CompanyID
										orderby oMajor.Description
															 select oMajor).ToList();
			oMajorList.Insert(0, new Major { MajorID = 0, Description = "Select" });
			ViewBag.ListOfMajor = oMajorList;
		}

		private void GetStatusList()
		{
			List<Status> oStatusList = new List<Status>();
			oStatusList = (from oStatus in oCATSDatabaseContext.Status
										 where oStatus.DeleteFlag == false && oStatus.CompanyID == Globals.CompanyID
										 orderby oStatus.SortOrder
										 select oStatus).ToList();
			//oStatusList.Insert(0, new Status { StatusID = 0, Description = "Select" });
			ViewBag.ListOfStatus = oStatusList;
		}

		private void GetGradeList()
		{
			List<Grade> oGradeList = new List<Grade>();
			oGradeList = (from oGrade in oCATSDatabaseContext.Grade
										 where oGrade.DeleteFlag == false && oGrade.CompanyID == Globals.CompanyID
										 orderby oGrade.Description
										 select oGrade).ToList();
			oGradeList.Insert(0, new Grade { GradeID = 0, Description = "Select" });
			ViewBag.ListOfGrade = oGradeList;
		}

		private void GetDeniedList()
		{
			List<Denied> oDeniedList = new List<Denied>();
			oDeniedList = (from oDenied in oCATSDatabaseContext.Denied
										 where oDenied.DeleteFlag == false && oDenied.CompanyID == Globals.CompanyID
										 orderby oDenied.Description
										select oDenied).ToList();
			oDeniedList.Insert(0, new Denied { DeniedID = 0, Description = "Select" });
			ViewBag.ListOfDenied = oDeniedList;
		}

		//private string GetAccountNumber(string pCostCenter, int pEducationalProgramID)
		//{
		//	string sAccountNumber;
		//	if (pCostCenter.Length < 9) { return string.Empty; }
		//	string sPrefix = pCostCenter.Substring(0, 3);
		//	string sDepartmentNumber = pCostCenter.Substring(pCostCenter.Length - 6, 6);

		//	switch (sPrefix)
		//	{
		//		case "150":
		//			if (pEducationalProgramID == 1 || pEducationalProgramID == 2)
		//			{
		//				sAccountNumber = "1506-854000-25500";
		//			}
		//			else
		//			{
		//				sAccountNumber = "1506-854000-20500";
		//			}
		//			break;
		//		case "550":
		//			if (pEducationalProgramID == 1 || pEducationalProgramID == 2)
		//			{
		//				sAccountNumber = "5506-" + sDepartmentNumber + "-72001";
		//			}
		//			else
		//			{
		//				sAccountNumber = "5506-" + sDepartmentNumber + "-72000";
		//			}
		//			break;
		//		case "120":
		//			if (pEducationalProgramID == 1 || pEducationalProgramID == 2)
		//			{
		//				sAccountNumber = "1206-854000-25500";
		//			}
		//			else
		//			{
		//				sAccountNumber = "1206-854000-20500";
		//			}
		//			break;
		//		case "200":
		//			if (pEducationalProgramID == 1 || pEducationalProgramID == 2)
		//			{
		//				sAccountNumber = "2006-854000-25500";
		//			}
		//			else
		//			{
		//				sAccountNumber = "2006-854000-20500";
		//			}
		//			break;
		//		case "600":
		//			sAccountNumber = "6006-" + sDepartmentNumber + "-20500";
		//			break;
		//		case "110":
		//			if (sDepartmentNumber.Substring(1, 3) == "108")
		//			{
		//				sAccountNumber = "1101-108000-70000";
		//			}
		//			else
		//			{
		//				if (pEducationalProgramID == 1 || pEducationalProgramID == 2)
		//				{
		//					sAccountNumber = "1106-854000-25500";
		//				}
		//				else
		//				{
		//					sAccountNumber = "1106-854000-20500";
		//				}
		//			}
		//			break;
		//		default:
		//			sAccountNumber = string.Empty;
		//			break;
		//	}
		//	return sAccountNumber;
		//}

		//private double GetReimbursementPercent(int pEducationalProgramID, string sEmployeeStatus)
		//{
		//	if (pEducationalProgramID == 3 || pEducationalProgramID == 5)
		//	{
		//		return 1;
		//	}
		//	switch (sEmployeeStatus)
		//	{
		//		case "01":
		//			return 1;
		//		case "25":
		//			return .5;
		//		default:
		//			return 0;
		//	}
		//}

		private double GetReimbursementPercent(int pEducationalProgramID, string pEmploymentCategoryCode)
		{
			if (pEducationalProgramID == 3 || pEducationalProgramID == 5)
			{
				return 1;
			}
			if (pEmploymentCategoryCode == null)
			{
				return 0;
			}
			switch (pEmploymentCategoryCode.ToLower())
			{
				case "fr":
					return 1;
				case "pr":
					return .5;
				default:
					return 0;
			}
		}

		//private double GetMaximumReimbursementAmount(string pPositionNumber, string sEmployeeStatus) 
		//{
		//	if (sEmployeeStatus != "01" && sEmployeeStatus != "25")
		//	{
		//		return 0;
		//	}
		//	Position oPosition = oCATSDatabaseContext.Position.SingleOrDefault(Position => Position.PositionNumber == pPositionNumber && Position.DeleteFlag == false);
		//	if (oPosition != null)
		//	{
		//			return 8000;
		//	}
		//	else
		//	{
		//		return 4000;
		//	}
		//}

		private double GetMaximumReimbursementAmount(string pJobCode, string sEmployeeStatus)
		{
			if (sEmployeeStatus.ToUpper() != "ACTIVE_PROCESS")
			{
				return 0;
			}
			JobCodeIR oJobCodeIR = oCATSDatabaseContext.JobCodeIR.SingleOrDefault(JobCodeIR => JobCodeIR.JobCode == pJobCode && JobCodeIR.DeleteFlag == false);
			if (oJobCodeIR != null)
			{
				return 8000;
			}
			else
			{
				return 4000;
			}
		}

		private string SetAccountNumber(string sCompanyID, string sDepartmentNumber)
		{
			if (sCompanyID.Trim() == "4110" || sCompanyID.Trim() == "9040")
			{
				return sCompanyID.Trim();
			}
			if (sCompanyID.Trim() == "4400")
			{
				return sCompanyID.Trim() + " " + sDepartmentNumber.Trim() + " 79110";
			}
			return sCompanyID.Trim() + " " + sDepartmentNumber.Trim() + " 62070";
		}

		private double GetYTDTuitionReceived(Int64 pEmployeeNumber, int pTuitionRequestID, string pJobCode, DateTime pCourseStartDate, int pEducationalProgramID)
		{
			double dReturnAmount = 0;
			List<TuitionRequest> oList = new List<TuitionRequest>();
			//Position oPosition = oCATSDatabaseContext.Position.SingleOrDefault(Position => Position.PositionNumber == pPositionNumber && Position.DeleteFlag == false);
			JobCodeIR oJobCodeIR = oCATSDatabaseContext.JobCodeIR.SingleOrDefault(JobCodeIR => JobCodeIR.JobCode == pJobCode && JobCodeIR.DeleteFlag == false);
			Boolean bSchoolYear = (oJobCodeIR == null);
			oList = (from oQuery in oCATSDatabaseContext.TuitionRequests
				where oQuery.DeleteFlag == false && oQuery.EmployeeNumber == pEmployeeNumber && oQuery.TuitionRequestID != pTuitionRequestID
				select oQuery).ToList();
			foreach (var TuitionRequest in oList)
			{
				if (TuitionRequest.EducationalProgramID == 1 || TuitionRequest.EducationalProgramID == 2)
				{
					if (bSchoolYear)
					{
						if (pCourseStartDate.Month >= 9 && pCourseStartDate.Month <= 12 &&
							((TuitionRequest.CourseStartDate.Month >= 9 && TuitionRequest.CourseStartDate.Month <= 12 && TuitionRequest.CourseStartDate.Year == pCourseStartDate.Year) ||
							(TuitionRequest.CourseStartDate.Month >= 1 && TuitionRequest.CourseStartDate.Month <= 8 && TuitionRequest.CourseStartDate.Year == pCourseStartDate.Year + 1)))
						{
							dReturnAmount += TuitionRequest.ReimbursementAmount;
						}

						if (pCourseStartDate.Month >= 1 && pCourseStartDate.Month <= 8 &&
							((TuitionRequest.CourseStartDate.Month >= 9 && TuitionRequest.CourseStartDate.Month <= 12 && TuitionRequest.CourseStartDate.Year == pCourseStartDate.Year - 1) ||
							(TuitionRequest.CourseStartDate.Month >= 1 && TuitionRequest.CourseStartDate.Month <= 8 && TuitionRequest.CourseStartDate.Year == pCourseStartDate.Year)))
						{
							dReturnAmount += TuitionRequest.ReimbursementAmount;
						}
					}
					else
					{
						if (TuitionRequest.CourseStartDate.Year == pCourseStartDate.Year)
						{
							dReturnAmount += TuitionRequest.ReimbursementAmount;
						}
					}
				}
			}
			return dReturnAmount;
		}

		private double GetYTDCertificationTuitionReceived(Int64 pEmployeeNumber, int pTuitionRequestID, string pJobCode, DateTime pCourseStartDate, int pEducationalProgramID)
		{
			double dReturnAmount = 0;
			List<TuitionRequest> oList = new List<TuitionRequest>();
			//Position oPosition = oCATSDatabaseContext.Position.SingleOrDefault(Position => Position.PositionNumber == pPositionNumber && Position.DeleteFlag == false);
			JobCodeIR oJobCodeIR = oCATSDatabaseContext.JobCodeIR.SingleOrDefault(JobCodeIR => JobCodeIR.JobCode == pJobCode && JobCodeIR.DeleteFlag == false);
			Boolean bSchoolYear = (oJobCodeIR == null);
			oList = (from oQuery in oCATSDatabaseContext.TuitionRequests
							 where oQuery.DeleteFlag == false && oQuery.EmployeeNumber == pEmployeeNumber && oQuery.TuitionRequestID != pTuitionRequestID
							 select oQuery).ToList();
			foreach (var TuitionRequest in oList)
			{
				if (TuitionRequest.EducationalProgramID == 3 || TuitionRequest.EducationalProgramID == 5)
				{
					if (bSchoolYear)
					{
						if (pCourseStartDate.Month >= 9 && pCourseStartDate.Month <= 12 &&
							((TuitionRequest.CourseStartDate.Month >= 9 && TuitionRequest.CourseStartDate.Month <= 12 && TuitionRequest.CourseStartDate.Year == pCourseStartDate.Year) ||
							(TuitionRequest.CourseStartDate.Month >= 1 && TuitionRequest.CourseStartDate.Month <= 8 && TuitionRequest.CourseStartDate.Year == pCourseStartDate.Year + 1)))
						{
							dReturnAmount += TuitionRequest.ReimbursementAmount;
						}

						if (pCourseStartDate.Month >= 1 && pCourseStartDate.Month <= 8 &&
							((TuitionRequest.CourseStartDate.Month >= 9 && TuitionRequest.CourseStartDate.Month <= 12 && TuitionRequest.CourseStartDate.Year == pCourseStartDate.Year - 1) ||
							(TuitionRequest.CourseStartDate.Month >= 1 && TuitionRequest.CourseStartDate.Month <= 8 && TuitionRequest.CourseStartDate.Year == pCourseStartDate.Year)))
						{
							dReturnAmount += TuitionRequest.ReimbursementAmount;
						}
					}
					else
					{
						if (TuitionRequest.CourseStartDate.Year == pCourseStartDate.Year)
						{
							dReturnAmount += TuitionRequest.ReimbursementAmount;
						}
					}
				}
			}
			return dReturnAmount;
		}

		private double ReimbursementAmount(double pCurrentTuitionAmount, double pFinancialAidAmount, double pYTDTuitionReceived, int pEducationalProgramID, double pReimbursementPercent, double pMaximumReimbursement)
		{
			if (pYTDTuitionReceived > pMaximumReimbursement)
			{
				return 0;
			}
			if (pEducationalProgramID == 3 || pEducationalProgramID == 5)
			{
				return pCurrentTuitionAmount;
			}
			double dReimbursementAmount = pCurrentTuitionAmount - pFinancialAidAmount;
			if (dReimbursementAmount <= 0)
			{
				return 0;
			}
			dReimbursementAmount = dReimbursementAmount * pReimbursementPercent;
			if ((pYTDTuitionReceived + dReimbursementAmount) > pMaximumReimbursement)
			{
				dReimbursementAmount = pMaximumReimbursement - pYTDTuitionReceived;
			}
			return dReimbursementAmount;
		}
		#endregion

		#region Index
		public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
		{
			if (sortOrder == null)
			{
				sortOrder = "RequestNumber";
			}

			ViewBag.CurrentSort = sortOrder;
			ViewBag.RequestNumberSortParm = sortOrder == "RequestNumber" ? "RequestNumber desc" : "RequestNumber";
			ViewBag.LastNameSortParm = sortOrder == "LastName" ? "LastName desc" : "LastName";
			ViewBag.FirstNameSortParm = sortOrder == "FirstName" ? "FirstName desc" : "FirstName";
			ViewBag.EmployeeNumberSortParm = sortOrder == "EmployeeNumber" ? "EmployeeNumber desc" : "EmployeeNumber";
			ViewBag.StatusDescriptionSortParm = sortOrder == "StatusDescription" ? "StatusDescription desc" : "StatusDescription";

			if (searchString != null)
			{
				page = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["currentFilter"] = searchString;

			var query = from oTuitionRequestDisplay in oCATSDatabaseContext.TuitionRequestDisplay
									where oTuitionRequestDisplay.CompanyID == Globals.CompanyID && oTuitionRequestDisplay.Closed == false
									select oTuitionRequestDisplay;

			if (!String.IsNullOrEmpty(searchString))
			{
				query = query.Where(select => select.LastName.Contains(searchString));
			}

			switch (sortOrder)
			{
				case "RequestNumber desc":
					query = query.OrderByDescending(select => select.RequestNumber);
					break;
				case "LastName":
					query = query.OrderBy(select => select.LastName);
					break;
				case "LastName desc":
					query = query.OrderByDescending(select => select.LastName);
					break;
				case "FirstName":
					query = query.OrderBy(select => select.FirstName);
					break;
				case "FirstName desc":
					query = query.OrderByDescending(select => select.FirstName);
					break;
				case "EmployeeNumber":
					query = query.OrderBy(select => select.EmployeeNumber);
					break;
				case "EmployeeNumber desc":
					query = query.OrderByDescending(select => select.EmployeeNumber);
					break;
				case "StatusDescription":
					query = query.OrderBy(select => select.StatusDescription);
					break;
				case "StatusDescription desc":
					query = query.OrderByDescending(select => select.StatusDescription);
					break;
				default:
					query = query.OrderBy(select => select.RequestNumber);
					break;
			}

			return View(await PaginatedList<TuitionRequestDisplay>.CreateAsync(query.AsNoTracking(), page ?? 1, Globals.PageSize));
		}
		#endregion

		#region Create/Edit
		[HttpGet("TuitionRequest/CreateEdit/{pEmployeeID:int}/{pNewFlag:int}")]
		public IActionResult CreateEdit(int pEmployeeID, int pNewFlag)
		{
			if (pEmployeeID == 0) {
				return NotFound();
			}

			string sTrackingGUID = Guid.NewGuid().ToString();
			var oRequestNumberTracking = new RequestNumberTracking
			{
				TrackingGUID = sTrackingGUID
			};
			oCATSDatabaseContext.Add(oRequestNumberTracking);
			oCATSDatabaseContext.SaveChanges();
			oRequestNumberTracking = oCATSDatabaseContext.RequestNumberTracking.SingleOrDefault(RequestNumberTracking => RequestNumberTracking.TrackingGUID == sTrackingGUID);

			var oEmployee = oOracleHRDatabaseContext.Employee.SingleOrDefault(Employee => Employee.EmployeeID == pEmployeeID);
			var oTuitionRequest = new TuitionRequest
			{
				CompanyID = Globals.CompanyID,
				RequestNumber = oRequestNumberTracking.RequestNumberTrackingID,
				PersonID = oEmployee.PersonID,
				EmployeeNumber = oEmployee.EmployeeNumber,
				LastName = oEmployee.LastName,
				FirstName = oEmployee.FirstName,
				//SocialSecurityNumber = oEmployee.SocialSecurityNumber,
				DateOfHire = oEmployee.DateOfHire,
				EmployeeStatus = oEmployee.EmployeeStatus,
				StatusDescription = oEmployee.StatusDescription,
				EmploymentCategoryCode = oEmployee.EmploymentCategoryCode,
				EmploymentCategoryName = oEmployee.EmploymentCategoryName,
				PositionNumber = oEmployee.PositionNumber,
				Position = oEmployee.Position,
				JobCode = oEmployee.JobCode,
				JobName = oEmployee.JobName,
				UnionCode = oEmployee.UnionCode,
				CostCenter = oEmployee.CostCenter,
				Entity = oEmployee.Entity,
				HomePhone = oEmployee.HomePhone,
				DateEntered = DateTime.Now.ToString("MM-dd-yyyy"),
				CourseStartDate = DateTime.ParseExact(DateTime.Now.ToString("MM-dd-yyyy"), "MM-dd-yyyy", CultureInfo.InvariantCulture),
				CourseEndDate = DateTime.ParseExact(DateTime.Now.ToString("MM-dd-yyyy"), "MM-dd-yyyy", CultureInfo.InvariantCulture),
				PaymentOptionID = 3,
				SendProcessingEmail = false,
				SendDeniedEmail = true,
				SendApprovedEmail = true,
				SendIncompleteEmail = true,
				ReimbursementPercent = 0,
				MaximumReimbursement = GetMaximumReimbursementAmount(oEmployee.JobCode, oEmployee.EmployeeStatus),
				YTDTuitionReceived = 0,
				ReimbursementAmount = 0,			
				AccountNumber = SetAccountNumber(oEmployee.CATSCompanyID, oEmployee.CATSDepartmentNumber),
				EMailAddress=oEmployee.WorkEMail,
				DeleteFlag = false
			};
			GetPaymentOptionList();
			GetFinancialAidTypeList();
			GetEducationalInstitutionList();
			GetEducationalProgramList();
			GetMajorList();
			GetStatusList();
			//GetPercentList();
			//GetSentToList();
			GetGradeList();
			GetDeniedList();
			ViewBag.CurrentMode = "create";
		return View(oTuitionRequest);
		}

		[HttpPost("TuitionRequest/CreateEdit/{pEmployeeID:int}/{pNewFlag:int}")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit([Bind("TuitionRequestID, CompanyID, RequestNumber, PersonID, EmployeeNumber, LastName, FirstName, DateOfHire, EmployeeStatus, StatusDescription, " +
			"EmploymentCategoryCode, EmploymentCategoryName, PositionNumber, Position, JobCode, JobName, UnionCode, CostCenter, Entity, HomePhone, EMailAddress, DateEntered, " +
			"EducationalInstitutionID, EducationalProgramID, MajorID, CourseStartDate, CourseEndDate, PaymentOptionID, CourseTitle1, TuitionAmount1, Grade1ID, CourseTitle2, TuitionAmount2, " +
			"Grade2ID, CourseTitle3, TuitionAmount3, Grade3ID, CourseTitle4, TuitionAmount4, Grade4ID, FinancialAidTypeID, FinancialAidAmount, StatusID, DeniedID, Comment, SendProcessingEmail, " +
			"ProcessingEmailSentDateTime, SendDeniedEmail, DeniedEmailSentDateTime, SendApprovedEmail, ApprovedEmailSentDateTime, SendIncompleteEmail, IncompleteEmailSentDateTime, CheckNumber, " +
			"CheckMailDate, CheckPickUpDate, ReimbursementPercent, MaximumReimbursement, YTDTuitionAmount, ReimbursementAmount, AccountNumber, Closed, DeleteFlag, RecordUpdate")] TuitionRequest oTuitionRequest)
		{
			if (ModelState.IsValid)
			{
				//oTuitionRequest.AccountNumber = GetAccountNumber(oTuitionRequest.CostCenter, oTuitionRequest.EducationalProgramID);
				oTuitionRequest.ReimbursementPercent = GetReimbursementPercent(oTuitionRequest.EducationalProgramID, oTuitionRequest.EmploymentCategoryCode);
				oTuitionRequest.MaximumReimbursement = GetMaximumReimbursementAmount(oTuitionRequest.JobCode, oTuitionRequest.EmployeeStatus);
				oTuitionRequest.YTDTuitionReceived = GetYTDTuitionReceived(oTuitionRequest.EmployeeNumber, 0, oTuitionRequest.JobCode, oTuitionRequest.CourseStartDate, oTuitionRequest.EducationalProgramID);
				double TuitionAmount = oTuitionRequest.TuitionAmount1 + oTuitionRequest.TuitionAmount2 + oTuitionRequest.TuitionAmount3 + oTuitionRequest.TuitionAmount4;
				oTuitionRequest.ReimbursementAmount = ReimbursementAmount(TuitionAmount, oTuitionRequest.FinancialAidAmount, oTuitionRequest.YTDTuitionReceived, oTuitionRequest.EducationalProgramID, oTuitionRequest.ReimbursementPercent, oTuitionRequest.MaximumReimbursement);
				if (oTuitionRequest.EducationalProgramID == 1 || oTuitionRequest.EducationalProgramID == 2)
				{
					oTuitionRequest.YTDTuitionReceived = oTuitionRequest.YTDTuitionReceived + oTuitionRequest.ReimbursementAmount;
				}
				oTuitionRequest.YTDTuitionReceived = oTuitionRequest.YTDTuitionReceived + oTuitionRequest.ReimbursementAmount;
				oTuitionRequest.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Add(oTuitionRequest);
				//try
				//{
				await oCATSDatabaseContext.SaveChangesAsync();
				//}
				//catch (Exception oException)
				//{
				//	int iwork = 0;
				//}
				return RedirectToAction(nameof(Index));
			}
			return View(oTuitionRequest);
		}

		//---------------------------------
		//---------------------------------

		[HttpGet("TuitionRequest/CreateEdit/{pTuitionRequestID:int}")]
		public async Task<IActionResult> CreateEdit(int pTuitionRequestID)
		{
			//if (pTuitionRequestID == null) {
			//	return NotFound();
			//}
			var oTuitionRequest = await oCATSDatabaseContext.TuitionRequests.SingleOrDefaultAsync(qTuitionRequest => qTuitionRequest.TuitionRequestID == pTuitionRequestID);
			if (oTuitionRequest == null)
			{
				return NotFound();
			}
			oTuitionRequest.YTDTuitionReceived= GetYTDTuitionReceived(oTuitionRequest.EmployeeNumber, pTuitionRequestID, oTuitionRequest.PositionNumber, oTuitionRequest.CourseStartDate, oTuitionRequest.EducationalProgramID);
			GetPaymentOptionList();
			GetFinancialAidTypeList();
			GetEducationalInstitutionList();
			GetEducationalProgramList();
			GetMajorList();
			GetStatusList();
			GetGradeList();
			GetDeniedList();
			ViewBag.CurrentMode = "edit";
			return View(oTuitionRequest);			
		}

		[HttpPost("TuitionRequest/CreateEdit/{pTuitionRequestID:int}")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit(int pTuitionRequestID, [Bind("TuitionRequestID, CompanyID, RequestNumber, PersonID, EmployeeNumber, LastName, FirstName, DateOfHire, EmploymentCategoryCode, " +
			"EmploymentCategoryName, EmployeeStatus, StatusDescription, PositionNumber, Position, JobCode, JobName, UnionCode, CostCenter, Entity, HomePhone, EMailAddress, DateEntered, EducationalInstitutionID, " +
			"EducationalProgramID, MajorID, CourseStartDate, CourseEndDate, PaymentOptionID, CourseTitle1, TuitionAmount1, Grade1ID, CourseTitle2, TuitionAmount2, Grade2ID, CourseTitle3, TuitionAmount3, " +
			"Grade3ID, CourseTitle4, TuitionAmount4, Grade4ID, FinancialAidTypeID, FinancialAidAmount, StatusID, DeniedID, Comment, SendProcessingEmail, ProcessingEmailSentDateTime, SendDeniedEmail, " + 
			"DeniedEmailSentDateTime, SendApprovedEmail, ApprovedEmailSentDateTime, SendIncompleteEmail, IncompleteEmailSentDateTime, CheckNumber, CheckMailDate, CheckPickUpDate, ReimbursementPercent, " +
			"MaximumReimbursement, YTDTuitionAmount, ReimbursementAmount, AccountNumber, Closed, DeleteFlag, RecordUpdate")] TuitionRequest oTuitionRequest)
		{
			if (ModelState.IsValid)
			{
				try
				{
					oTuitionRequest.TuitionRequestID = pTuitionRequestID;
//					oTuitionRequest.AccountNumber = GetAccountNumber(oTuitionRequest.CostCenter, oTuitionRequest.EducationalProgramID);
					oTuitionRequest.ReimbursementPercent = GetReimbursementPercent(oTuitionRequest.EducationalProgramID, oTuitionRequest.EmploymentCategoryCode);
					oTuitionRequest.MaximumReimbursement = GetMaximumReimbursementAmount(oTuitionRequest.JobCode, oTuitionRequest.EmployeeStatus);
					oTuitionRequest.YTDTuitionReceived = GetYTDTuitionReceived(oTuitionRequest.EmployeeNumber, pTuitionRequestID, oTuitionRequest.PositionNumber,oTuitionRequest.CourseStartDate,oTuitionRequest.EducationalProgramID);
					double TuitionAmount = oTuitionRequest.TuitionAmount1 + oTuitionRequest.TuitionAmount2 + oTuitionRequest.TuitionAmount3 + oTuitionRequest.TuitionAmount4;
					oTuitionRequest.ReimbursementAmount = ReimbursementAmount(TuitionAmount, oTuitionRequest.FinancialAidAmount, oTuitionRequest.YTDTuitionReceived, oTuitionRequest.EducationalProgramID, oTuitionRequest.ReimbursementPercent, oTuitionRequest.MaximumReimbursement);
					if (oTuitionRequest.EducationalProgramID == 1 || oTuitionRequest.EducationalProgramID == 2)
					{
						oTuitionRequest.YTDTuitionReceived = oTuitionRequest.YTDTuitionReceived + oTuitionRequest.ReimbursementAmount;
					}					
					oTuitionRequest.RecordUpdate = DateTime.Now;
					oCATSDatabaseContext.Update(oTuitionRequest);
					await oCATSDatabaseContext.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!TuitionRequestExists(oTuitionRequest.TuitionRequestID))
					{ return NotFound(); }
					else
					{ throw; }
				}
				return RedirectToAction(nameof(Index));
			}
			return View(oTuitionRequest);
		}
		#endregion

		#region EmployeeIndex
		public async Task<IActionResult> EmployeeIndex(string sortOrder, string currentFilter, string searchString, int? page)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.LastNameSortParm = sortOrder == "LastName" ? "LastName desc" : "LastName";
			ViewBag.FirstNameSortParm = sortOrder == "FirstName" ? "FirstName desc" : "FirstName";
			ViewBag.EmployeeNumberSortParm = sortOrder == "EmployeeNumber" ? "EmployeeNumber desc" : "EmployeeNumber";

			if (searchString != null)
			{
				page = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["currentFilter"] = searchString;

			var query = from oEmployee in oOracleHRDatabaseContext.Employee
										//where oEmployee.DeleteFlag == false 
									select oEmployee;

			if (!String.IsNullOrEmpty(searchString))
			{
				query = query.Where(select => select.LastName.Contains(searchString));
			}

			switch (sortOrder)
			{
				case "LastName desc":
					query = query.OrderByDescending(select => select.LastName);
					break;
				case "FirstName":
					query = query.OrderBy(select => select.FirstName);
					break;
				case "FirstName desc":
					query = query.OrderByDescending(select => select.FirstName);
					break;
				case "EmployeeNumber":
					query = query.OrderBy(select => select.EmployeeNumber);
					break;
				case "EmployeeNumber desc":
					query = query.OrderByDescending(select => select.EmployeeNumber);
					break;
				default:
					query = query.OrderBy(select => select.LastName);
					break;
			}

			return View(await PaginatedList<Employee>.CreateAsync(query.AsNoTracking(), page ?? 1, Globals.PageSize));
		}
		#endregion

		#region EmployeeDetails
		[HttpGet("TuitionRequest/EmployeeDetails/{pEmployeeID:int}")]
		public async Task<IActionResult> EmployeeDetails(int? pEmployeeID)
		{
			if (pEmployeeID == null)
			{
				return NotFound();
			}

			var query = await oOracleHRDatabaseContext.Employee
					.SingleOrDefaultAsync(select => select.EmployeeID == pEmployeeID);
			if (query == null)
			{
				return NotFound();
			}

			return View(query);
		}
		#endregion

		#region Delete
		[HttpGet("TuitionRequest/Delete/{pTuitionRequestID:int}")]
		public async Task<IActionResult> Delete(int? pTuitionRequestID)
		{
			if (pTuitionRequestID == null) { return NotFound(); }
			var oTuitionRequest = await oCATSDatabaseContext.TuitionRequests.SingleOrDefaultAsync(qTuitionRequest => qTuitionRequest.TuitionRequestID == pTuitionRequestID);
			if (oTuitionRequest == null) { return NotFound(); }
			return View(oTuitionRequest);
		}

		[HttpPost("TuitionRequest/Delete/{pTuitionRequestID:int}"), ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int? pTuitionRequestID)
		{
			if (pTuitionRequestID == null) { return NotFound(); }
			var oTuitionRequest = await oCATSDatabaseContext.TuitionRequests.SingleOrDefaultAsync(qTuitionRequest => qTuitionRequest.TuitionRequestID == pTuitionRequestID);
			if (oTuitionRequest == null) { return NotFound(); }
			try
			{
				oTuitionRequest.DeleteFlag = true;
				oTuitionRequest.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Update(oTuitionRequest);
				await oCATSDatabaseContext.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!TuitionRequestExists(oTuitionRequest.TuitionRequestID))
				{ return NotFound(); }
				else
				{ throw; }
			}
			return RedirectToAction(nameof(Index));
		}
		#endregion

		#region IndexClosed
		public async Task<IActionResult> IndexClosed(string sortOrder, string currentFilter, string searchString, int? page)
		{
			if (sortOrder == null)
			{
				sortOrder = "RequestNumber";
			}

			ViewBag.CurrentSort = sortOrder;
			ViewBag.RequestNumberSortParm = sortOrder == "RequestNumber" ? "RequestNumber desc" : "RequestNumber";
			ViewBag.LastNameSortParm = sortOrder == "LastName" ? "LastName desc" : "LastName";
			ViewBag.FirstNameSortParm = sortOrder == "FirstName" ? "FirstName desc" : "FirstName";
			ViewBag.EmployeeNumberSortParm = sortOrder == "EmployeeNumber" ? "EmployeeNumber desc" : "EmployeeNumber";
			ViewBag.StatusDescriptionSortParm = sortOrder == "StatusDescription" ? "StatusDescription desc" : "StatusDescription";

			if (searchString != null)
			{
				page = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["currentFilter"] = searchString;

			var query = from oTuitionRequestDisplay in oCATSDatabaseContext.TuitionRequestDisplay
									where oTuitionRequestDisplay.CompanyID == Globals.CompanyID && oTuitionRequestDisplay.Closed == true
									select oTuitionRequestDisplay;

			if (!String.IsNullOrEmpty(searchString))
			{
				query = query.Where(select => select.LastName.Contains(searchString));
			}

			switch (sortOrder)
			{
				case "RequestNumber desc":
					query = query.OrderByDescending(select => select.RequestNumber);
					break;
				case "LastName":
					query = query.OrderBy(select => select.LastName);
					break;
				case "LastName desc":
					query = query.OrderByDescending(select => select.LastName);
					break;
				case "FirstName":
					query = query.OrderBy(select => select.FirstName);
					break;
				case "FirstName desc":
					query = query.OrderByDescending(select => select.FirstName);
					break;
				case "EmployeeNumber":
					query = query.OrderBy(select => select.EmployeeNumber);
					break;
				case "EmployeeNumber desc":
					query = query.OrderByDescending(select => select.EmployeeNumber);
					break;
				case "StatusDescription":
					query = query.OrderBy(select => select.StatusDescription);
					break;
				case "StatusDescription desc":
					query = query.OrderByDescending(select => select.StatusDescription);
					break;
				default:
					query = query.OrderBy(select => select.RequestNumber);
					break;
			}

			return View(await PaginatedList<TuitionRequestDisplay>.CreateAsync(query.AsNoTracking(), page ?? 1, Globals.PageSize));
		}
		#endregion

		#region Details
		[HttpGet("TuitionRequest/Details/{pTuitionRequestID:int}")]
		public async Task<IActionResult> Details(int pTuitionRequestID) 
		{
			try
			{
				var oTuitionRequestClosed = await oCATSDatabaseContext.TuitionRequestsClosed.SingleOrDefaultAsync(qTuitionRequestClosed => qTuitionRequestClosed.TuitionRequestID == pTuitionRequestID);
				oTuitionRequestClosed.YTDTuitionReceived = GetYTDTuitionReceived(oTuitionRequestClosed.EmployeeNumber, pTuitionRequestID, oTuitionRequestClosed.PositionNumber, oTuitionRequestClosed.CourseStartDate, oTuitionRequestClosed.EducationalProgramID);
				oTuitionRequestClosed.YTDCertificationTuitionReceived = GetYTDCertificationTuitionReceived(oTuitionRequestClosed.EmployeeNumber, pTuitionRequestID, oTuitionRequestClosed.PositionNumber, oTuitionRequestClosed.CourseStartDate, oTuitionRequestClosed.EducationalProgramID);
					if (oTuitionRequestClosed == null)
				{
					return NotFound();
				}
			//GetPaymentOptionList();
			//GetFinancialAidTypeList();
			//GetEducationalInstitutionList(); 
			//GetEducationalProgramList();
			//GetMajorList();
			//GetStatusList();
			//GetGradeList();
			//GetDeniedList();
			//ViewBag.CurrentMode = "edit";
				return View(oTuitionRequestClosed);
			}
			catch (Exception oException)
			{
					return NotFound();
			}
		}
		#endregion
	}
}
