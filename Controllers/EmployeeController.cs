﻿#region Init
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CATS.Data;
using CATS.Models;
#endregion

namespace CATS.Controllers
{
	public class EmployeeController : Controller
	{
		#region Declarations
		private readonly OracleHRDatabaseContext oEmployeeContext;

		public EmployeeController(OracleHRDatabaseContext context)
		{ oEmployeeContext = context; }
		#endregion

		#region Index
		public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.LastNameSortParm = sortOrder == "LastName" ? "LastName desc" : "LastName";
			ViewBag.FirstNameSortParm = sortOrder == "FirstName" ? "FirstName desc" : "FirstName";
			ViewBag.EmployeeNumberSortParm = sortOrder == "EmployeeNumber" ? "EmployeeNumber desc" : "EmployeeNumber";

			if (searchString != null)
			{
				page = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["currentFilter"] = searchString;

			var query = from oEmployee in oEmployeeContext.Employee
									//where oEmployee.DeleteFlag == false 
									select oEmployee;

			if (!String.IsNullOrEmpty(searchString))
			{
				query = query.Where(select => select.LastName.Contains(searchString));
			}

			switch (sortOrder)
			{
				case "LastName desc":
					query = query.OrderByDescending(select => select.LastName);
					break;
				case "FirstName":
					query = query.OrderBy(select => select.FirstName);
					break;
				case "FirstName desc":
					query = query.OrderByDescending(select => select.FirstName);
					break;
				case "EmployeeNumber":
					query = query.OrderBy(select => select.EmployeeNumber);
					break;
				case "EmployeeNumber desc":
					query = query.OrderByDescending(select => select.EmployeeNumber);
					break;
				default:
					query = query.OrderBy(select => select.LastName);
					break;
			}

			return View(await PaginatedList<Employee>.CreateAsync(query.AsNoTracking(), page ?? 1, Globals.PageSize));
		}
		#endregion

		#region "Details"
		[HttpGet("Employee/Details/{pEmployeeID:int}")]
		public async Task<IActionResult> Details(int? pEmployeeID)
		{
			if (pEmployeeID == null)
			{
				return NotFound();
			}

			var query = await oEmployeeContext.Employee
					.SingleOrDefaultAsync(select => select.EmployeeID == pEmployeeID);
			if (query == null)
			{
				return NotFound();
			}

			return View(query);
		}
		#endregion
	}
}
