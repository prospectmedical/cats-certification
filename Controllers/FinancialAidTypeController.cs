﻿#region Init
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CATS.Data;
using CATS.Models;
#endregion

namespace CATS.Controllers
{
	public class FinancialAidTypeController : Controller
	{
		#region Declarations
		private readonly CATSDatabaseContext oCATSDatabaseContext;

		public FinancialAidTypeController(CATSDatabaseContext context)
		{ oCATSDatabaseContext = context; }
		#endregion

		#region Routines
		private bool FinancialAidTypeExists(int pFinancialAidTypeID)
		{ return oCATSDatabaseContext.FinancialAidType.Any(oFinancialAidType => oFinancialAidType.FinancialAidTypeID == pFinancialAidTypeID); }
		#endregion

		#region Index
		public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.DescriptionSortParm = sortOrder == "Description" ? "Description desc" : "Description";

			if (searchString != null)
			{
				page = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["currentFilter"] = searchString;

			var query = from oFinancialAidType in oCATSDatabaseContext.FinancialAidType
									where oFinancialAidType.DeleteFlag == false && oFinancialAidType.CompanyID == Globals.CompanyID
									select oFinancialAidType;

			if (!String.IsNullOrEmpty(searchString))
			{
				query = query.Where(select => select.Description.Contains(searchString));
			}
			switch (sortOrder)
			{
				case "Description desc":
					query = query.OrderByDescending(select => select.Description);
					break;
				default:
					query = query.OrderBy(select => select.Description);
					break;
			}

			return View(await PaginatedList<FinancialAidType>.CreateAsync(query.AsNoTracking(), page ?? 1, Globals.PageSize));
		}
		#endregion

		#region CreateEdit
		public IActionResult CreateEdit()
		{
			var oFinancialAidType = new FinancialAidType
			{
				FinancialAidTypeID = 0,
				CompanyID = Globals.CompanyID,
				DeleteFlag = false
			};
			return View(oFinancialAidType);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit([Bind("FinancialAidTypeID, CompanyID, Description, DeleteFlag, RecordUpdate")] FinancialAidType oFinancialAidType)
		{
			if (ModelState.IsValid)
			{
				oFinancialAidType.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Add(oFinancialAidType);
				await oCATSDatabaseContext.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(oFinancialAidType);
		}

		[HttpGet("FinancialAidType/CreateEdit/{pFinancialAidTypeID:int}")]
		public async Task<IActionResult> CreateEdit(int? pFinancialAidTypeID)
		{
			if (pFinancialAidTypeID == null) { return NotFound(); }
			var oFinancialAidType = await oCATSDatabaseContext.FinancialAidType.SingleOrDefaultAsync(qFinancialAidType => qFinancialAidType.FinancialAidTypeID == pFinancialAidTypeID);
			if (oFinancialAidType == null) { return NotFound(); }
			return View(oFinancialAidType);
		}

		[HttpPost("FinancialAidType/CreateEdit/{pFinancialAidTypeID:int}")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit(int pFinancialAidTypeID, [Bind("FinancialAidTypeID, CompanyID, Description, DeleteFlag, RecordUpdate")] FinancialAidType oFinancialAidType)
		{
			if (ModelState.IsValid)
			{
				try
				{
					oFinancialAidType.FinancialAidTypeID = pFinancialAidTypeID;
					oFinancialAidType.RecordUpdate = DateTime.Now;
					oCATSDatabaseContext.Update(oFinancialAidType);
					await oCATSDatabaseContext.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!FinancialAidTypeExists(oFinancialAidType.FinancialAidTypeID))
					{ return NotFound(); }
					else
					{ throw; }
				}
				return RedirectToAction(nameof(Index));
			}
			return View(oFinancialAidType);
		}
		#endregion

		#region Delete
		[HttpGet("FinancialAidType/Delete/{pFinancialAidTypeID:int}")]
		public async Task<IActionResult> Delete(int? pFinancialAidTypeID)
		{
			if (pFinancialAidTypeID == null) { return NotFound(); }
			var oFinancialAidType = await oCATSDatabaseContext.FinancialAidType.SingleOrDefaultAsync(qFinancialAidType => qFinancialAidType.FinancialAidTypeID == pFinancialAidTypeID);
			if (oFinancialAidType == null) { return NotFound(); }
			return View(oFinancialAidType);
		}

		[HttpPost("FinancialAidType/Delete/{pFinancialAidTypeID:int}"), ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int? pFinancialAidTypeID)
		{
			if (pFinancialAidTypeID == null) { return NotFound(); }
			var oFinancialAidType = await oCATSDatabaseContext.FinancialAidType.SingleOrDefaultAsync(qFinancialAidType => qFinancialAidType.FinancialAidTypeID == pFinancialAidTypeID);
			if (oFinancialAidType == null) { return NotFound(); }
			try
			{
				oFinancialAidType.DeleteFlag = true;
				oFinancialAidType.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Update(oFinancialAidType);
				await oCATSDatabaseContext.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!FinancialAidTypeExists(oFinancialAidType.FinancialAidTypeID))
				{ return NotFound(); }
				else
				{ throw; }
			}
			return RedirectToAction(nameof(Index));
		}
		#endregion
	}
}
