﻿#region Init
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CATS.Data;
using CATS.Models;
#endregion

namespace CATS.Controllers
{
	public class MajorController : Controller
	{
		#region Declarations
		private readonly CATSDatabaseContext oCATSDatabaseContext;

		public MajorController(CATSDatabaseContext context)
		{ oCATSDatabaseContext = context; }
		#endregion

		#region Routines
		private bool MajorExists(int pMajorID)
		{ return oCATSDatabaseContext.Major.Any(oMajor => oMajor.MajorID == pMajorID); }
		#endregion

		#region Index
		public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.DescriptionSortParm = sortOrder == "Description" ? "Description desc" : "Description";

			if (searchString != null)
			{
				page = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["currentFilter"] = searchString;

			var query = from oMajor in oCATSDatabaseContext.Major
									where oMajor.DeleteFlag == false && oMajor.CompanyID == Globals.CompanyID
									select oMajor;

			if (!String.IsNullOrEmpty(searchString))
			{
				query = query.Where(select => select.Description.Contains(searchString));
			}
			switch (sortOrder)
			{
				case "Description desc":
					query = query.OrderByDescending(select => select.Description);
					break;
				default:
					query = query.OrderBy(select => select.Description);
					break;
			}

			return View(await PaginatedList<Major>.CreateAsync(query.AsNoTracking(), page ?? 1, Globals.PageSize));
		}
		#endregion

		#region CreateEdit
		public IActionResult CreateEdit()
		{
			var oMajor = new Major
			{
				MajorID = 0,
				CompanyID = Globals.CompanyID,
				DeleteFlag = false
			};
			return View(oMajor);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit([Bind("MajorID, CompanyID, Description, DeleteFlag, RecordUpdate")] Major oMajor)
		{
			if (ModelState.IsValid)
			{
				oMajor.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Add(oMajor);
				await oCATSDatabaseContext.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(oMajor);
		}

		[HttpGet("Major/CreateEdit/{pMajorID:int}")]
		public async Task<IActionResult> CreateEdit(int? pMajorID)
		{
			if (pMajorID == null) { return NotFound(); }
			var oMajor = await oCATSDatabaseContext.Major.SingleOrDefaultAsync(qMajor => qMajor.MajorID == pMajorID);
			if (oMajor == null) { return NotFound(); }
			return View(oMajor);
		}

		[HttpPost("Major/CreateEdit/{pMajorID:int}")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit(int pMajorID, [Bind("MajorID, CompanyID, Description, DeleteFlag, RecordUpdate")] Major oMajor)
		{
			if (ModelState.IsValid)
			{
				try
				{
					oMajor.MajorID = pMajorID;
					oMajor.RecordUpdate = DateTime.Now;
					oCATSDatabaseContext.Update(oMajor);
					await oCATSDatabaseContext.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!MajorExists(oMajor.MajorID))
					{ return NotFound(); }
					else
					{ throw; }
				}
				return RedirectToAction(nameof(Index));
			}
			return View(oMajor);
		}
		#endregion

		#region Delete
		[HttpGet("Major/Delete/{pMajorID:int}")]
		public async Task<IActionResult> Delete(int? pMajorID)
		{
			if (pMajorID == null) { return NotFound(); }
			var oMajor = await oCATSDatabaseContext.Major.SingleOrDefaultAsync(qMajor => qMajor.MajorID == pMajorID);
			if (oMajor == null) { return NotFound(); }
			return View(oMajor);
		}

		[HttpPost("Major/Delete/{pMajorID:int}"), ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int? pMajorID)
		{
			if (pMajorID == null) { return NotFound(); }
			var oMajor = await oCATSDatabaseContext.Major.SingleOrDefaultAsync(qMajor => qMajor.MajorID == pMajorID);
			if (oMajor == null) { return NotFound(); }
			try
			{
				oMajor.DeleteFlag = true;
				oMajor.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Update(oMajor);
				await oCATSDatabaseContext.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!MajorExists(oMajor.MajorID))
				{ return NotFound(); }
				else
				{ throw; }
			}
			return RedirectToAction(nameof(Index));
		}
		#endregion
	}
}
