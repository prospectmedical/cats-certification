﻿#region Init
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CATS.Data;
using CATS.Models;
#endregion

namespace CATS.Controllers
{
	public class DeniedController : Controller
	{
		#region Declarations
		private readonly CATSDatabaseContext oCATSDatabaseContext;

		public DeniedController(CATSDatabaseContext context)
		{ oCATSDatabaseContext = context; }
		#endregion

		#region Routines
		private bool DeniedExists(int pDeniedID)
		{ return oCATSDatabaseContext.Denied.Any(oDenied => oDenied.DeniedID == pDeniedID); }
		#endregion

		#region Index
		public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.DescriptionSortParm = sortOrder == "Description" ? "Description desc" : "Description";

			if (searchString != null)
			{
				page = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["currentFilter"] = searchString;

			var query = from oDenied in oCATSDatabaseContext.Denied
									where oDenied.DeleteFlag == false && oDenied.CompanyID == Globals.CompanyID
									select oDenied;

			if (!String.IsNullOrEmpty(searchString))
			{
				query = query.Where(select => select.Description.Contains(searchString));
			}
			switch (sortOrder)
			{
				case "Description desc":
					query = query.OrderByDescending(select => select.Description);
					break;
				default:
					query = query.OrderBy(select => select.Description);
					break;
			}

			return View(await PaginatedList<Denied>.CreateAsync(query.AsNoTracking(), page ?? 1, Globals.PageSize));
		}
		#endregion

		#region CreateEdit
		public IActionResult CreateEdit()
		{
			var oDenied = new Denied
			{
				DeniedID = 0,
				CompanyID = Globals.CompanyID,
				DeleteFlag = false
			};
			return View(oDenied);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit([Bind("DeniedID, CompanyID, Description, DeleteFlag, RecordUpdate")] Denied oDenied)
		{
			if (ModelState.IsValid)
			{
				oDenied.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Add(oDenied);
				await oCATSDatabaseContext.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(oDenied);
		}

		[HttpGet("Denied/CreateEdit/{pDeniedID:int}")]
		public async Task<IActionResult> CreateEdit(int? pDeniedID)
		{
			if (pDeniedID == null) { return NotFound(); }
			var oDenied = await oCATSDatabaseContext.Denied.SingleOrDefaultAsync(qDenied => qDenied.DeniedID == pDeniedID);
			if (oDenied == null) { return NotFound(); }
			return View(oDenied);
		}

		[HttpPost("Denied/CreateEdit/{pDeniedID:int}")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit(int pDeniedID, [Bind("DeniedID, CompanyID, Description, DeleteFlag, RecordUpdate")] Denied oDenied)
		{
			if (ModelState.IsValid)
			{
				try
				{
					oDenied.DeniedID = pDeniedID;
					oDenied.RecordUpdate = DateTime.Now;
					oCATSDatabaseContext.Update(oDenied);
					await oCATSDatabaseContext.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!DeniedExists(oDenied.DeniedID))
					{ return NotFound(); }
					else
					{ throw; }
				}
				return RedirectToAction(nameof(Index));
			}
			return View(oDenied);
		}
		#endregion

		#region Delete
		[HttpGet("Denied/Delete/{pDeniedID:int}")]
		public async Task<IActionResult> Delete(int? pDeniedID)
		{
			if (pDeniedID == null) { return NotFound(); }
			var oDenied = await oCATSDatabaseContext.Denied.SingleOrDefaultAsync(qDenied => qDenied.DeniedID == pDeniedID);
			if (oDenied == null) { return NotFound(); }
			return View(oDenied);
		}

		[HttpPost("Denied/Delete/{pDeniedID:int}"), ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int? pDeniedID)
		{
			if (pDeniedID == null) { return NotFound(); }
			var oDenied = await oCATSDatabaseContext.Denied.SingleOrDefaultAsync(qDenied => qDenied.DeniedID == pDeniedID);
			if (oDenied == null) { return NotFound(); }
			try
			{
				oDenied.DeleteFlag = true;
				oDenied.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Update(oDenied);
				await oCATSDatabaseContext.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!DeniedExists(oDenied.DeniedID))
				{ return NotFound(); }
				else
				{ throw; }
			}
			return RedirectToAction(nameof(Index));
		}
		#endregion
	}
}
