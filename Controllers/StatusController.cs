﻿#region Init
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CATS.Data;
using CATS.Models;
#endregion

namespace CATS.Controllers
{
	public class StatusController : Controller
	{
		#region Declarations
		private readonly CATSDatabaseContext oCATSDatabaseContext;

		public StatusController(CATSDatabaseContext context)
		{ oCATSDatabaseContext = context; }
		#endregion

		#region Routines
		private bool StatusExists(int pStatusID)
		{ return oCATSDatabaseContext.Status.Any(oStatus => oStatus.StatusID == pStatusID); }
		#endregion

		#region Index
		public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.DescriptionSortParm = sortOrder == "Description" ? "Description desc" : "Description";

			if (searchString != null)
			{
				page = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["currentFilter"] = searchString;

			var query = from oStatus in oCATSDatabaseContext.Status
									where oStatus.DeleteFlag == false && oStatus.CompanyID == Globals.CompanyID
									select oStatus;

			if (!String.IsNullOrEmpty(searchString))
			{
				query = query.Where(select => select.Description.Contains(searchString));
			}
			switch (sortOrder)
			{
				case "Description desc":
					query = query.OrderByDescending(select => select.Description);
					break;
				default:
					query = query.OrderBy(select => select.Description);
					break;
			}

			return View(await PaginatedList<Status>.CreateAsync(query.AsNoTracking(), page ?? 1, Globals.PageSize));
		}
		#endregion

		#region CreateEdit
		public IActionResult CreateEdit()
		{
			var oStatus = new Status
			{
				StatusID = 0,
				CompanyID = Globals.CompanyID,
				DeleteFlag = false
			};
			return View(oStatus);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit([Bind("StatusID, CompanyID, Description, DeleteFlag, RecordUpdate")] Status oStatus)
		{
			if (ModelState.IsValid)
			{
				oStatus.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Add(oStatus);
				await oCATSDatabaseContext.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(oStatus);
		}

		[HttpGet("Status/CreateEdit/{pStatusID:int}")]
		public async Task<IActionResult> CreateEdit(int? pStatusID)
		{
			if (pStatusID == null) { return NotFound(); }
			var oStatus = await oCATSDatabaseContext.Status.SingleOrDefaultAsync(qStatus => qStatus.StatusID == pStatusID);
			if (oStatus == null) { return NotFound(); }
			return View(oStatus);
		}

		[HttpPost("Status/CreateEdit/{pStatusID:int}")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit(int pStatusID, [Bind("StatusID, CompanyID, Description, DeleteFlag, RecordUpdate")] Status oStatus)
		{
			if (ModelState.IsValid)
			{
				try
				{
					oStatus.StatusID = pStatusID;
					oStatus.RecordUpdate = DateTime.Now;
					oCATSDatabaseContext.Update(oStatus);
					await oCATSDatabaseContext.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!StatusExists(oStatus.StatusID))
					{ return NotFound(); }
					else
					{ throw; }
				}
				return RedirectToAction(nameof(Index));
			}
			return View(oStatus);
		}
		#endregion

		#region Delete
		[HttpGet("Status/Delete/{pStatusID:int}")]
		public async Task<IActionResult> Delete(int? pStatusID)
		{
			if (pStatusID == null) { return NotFound(); }
			var oStatus = await oCATSDatabaseContext.Status.SingleOrDefaultAsync(qStatus => qStatus.StatusID == pStatusID);
			if (oStatus == null) { return NotFound(); }
			return View(oStatus);
		}

		[HttpPost("Status/Delete/{pStatusID:int}"), ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int? pStatusID)
		{
			if (pStatusID == null) { return NotFound(); }
			var oStatus = await oCATSDatabaseContext.Status.SingleOrDefaultAsync(qStatus => qStatus.StatusID == pStatusID);
			if (oStatus == null) { return NotFound(); }
			try
			{
				oStatus.DeleteFlag = true;
				oStatus.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Update(oStatus);
				await oCATSDatabaseContext.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!StatusExists(oStatus.StatusID))
				{ return NotFound(); }
				else
				{ throw; }
			}
			return RedirectToAction(nameof(Index));
		}
		#endregion
	}
}
