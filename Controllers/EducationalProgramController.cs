﻿#region Init
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CATS.Data;
using CATS.Models;
#endregion

namespace CATS.Controllers
{
	public class EducationalProgramController : Controller
	{
		#region Declarations
		private readonly CATSDatabaseContext oCATSDatabaseContext;

		public EducationalProgramController(CATSDatabaseContext context)
		{ oCATSDatabaseContext = context; }
		#endregion

		#region Routines
		private bool EducationalProgramExists(int pEducationalProgramID)
		{ return oCATSDatabaseContext.EducationalProgram.Any(oEducationalProgram => oEducationalProgram.EducationalProgramID == pEducationalProgramID); }
		#endregion

		#region Index
		public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.DescriptionSortParm = sortOrder == "Description" ? "Description desc" : "Description";

			if (searchString != null)
			{
				page = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["currentFilter"] = searchString;

			var query = from oEducationalProgram in oCATSDatabaseContext.EducationalProgram
									where oEducationalProgram.DeleteFlag == false && oEducationalProgram.CompanyID == Globals.CompanyID
									select oEducationalProgram;

			if (!String.IsNullOrEmpty(searchString))
			{
				query = query.Where(select => select.Description.Contains(searchString));
			}
			switch (sortOrder)
			{
				case "Description desc":
					query = query.OrderByDescending(select => select.Description);
					break;
				default:
					query = query.OrderBy(select => select.Description);
					break;
			}

			return View(await PaginatedList<EducationalProgram>.CreateAsync(query.AsNoTracking(), page ?? 1, Globals.PageSize));
		}
		#endregion

		#region CreateEdit
		public IActionResult CreateEdit()
		{
			var oEducationalProgram = new EducationalProgram
			{
				EducationalProgramID = 0,
				CompanyID = Globals.CompanyID,
				DeleteFlag = false
			};
			return View(oEducationalProgram);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit([Bind("EducationalProgramID, CompanyID, Description, DeleteFlag, RecordUpdate")] EducationalProgram oEducationalProgram)
		{
			if (ModelState.IsValid)
			{
				oEducationalProgram.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Add(oEducationalProgram);
				await oCATSDatabaseContext.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(oEducationalProgram);
		}

		[HttpGet("EducationalProgram/CreateEdit/{pEducationalProgramID:int}")]
		public async Task<IActionResult> CreateEdit(int? pEducationalProgramID)
		{
			if (pEducationalProgramID == null) { return NotFound(); }
			var oEducationalProgram = await oCATSDatabaseContext.EducationalProgram.SingleOrDefaultAsync(qEducationalProgram => qEducationalProgram.EducationalProgramID == pEducationalProgramID);
			if (oEducationalProgram == null) { return NotFound(); }
			return View(oEducationalProgram);
		}

		[HttpPost("EducationalProgram/CreateEdit/{pEducationalProgramID:int}")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit(int pEducationalProgramID, [Bind("EducationalProgramID, CompanyID, Description, DeleteFlag, RecordUpdate")] EducationalProgram oEducationalProgram)
		{
			if (ModelState.IsValid)
			{
				try
				{
					oEducationalProgram.EducationalProgramID = pEducationalProgramID;
					oEducationalProgram.RecordUpdate = DateTime.Now;
					oCATSDatabaseContext.Update(oEducationalProgram);
					await oCATSDatabaseContext.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!EducationalProgramExists(oEducationalProgram.EducationalProgramID))
					{ return NotFound(); }
					else
					{ throw; }
				}
				return RedirectToAction(nameof(Index));
			}
			return View(oEducationalProgram);
		}
		#endregion

		#region Delete
		[HttpGet("EducationalProgram/Delete/{pEducationalProgramID:int}")]
		public async Task<IActionResult> Delete(int? pEducationalProgramID)
		{
			if (pEducationalProgramID == null) { return NotFound(); }
			var oEducationalProgram = await oCATSDatabaseContext.EducationalProgram.SingleOrDefaultAsync(qEducationalProgram => qEducationalProgram.EducationalProgramID == pEducationalProgramID);
			if (oEducationalProgram == null) { return NotFound(); }
			return View(oEducationalProgram);
		}

		[HttpPost("EducationalProgram/Delete/{pEducationalProgramID:int}"), ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int? pEducationalProgramID)
		{
			if (pEducationalProgramID == null) { return NotFound(); }
			var oEducationalProgram = await oCATSDatabaseContext.EducationalProgram.SingleOrDefaultAsync(qEducationalProgram => qEducationalProgram.EducationalProgramID == pEducationalProgramID);
			if (oEducationalProgram == null) { return NotFound(); }
			try
			{
				oEducationalProgram.DeleteFlag = true;
				oEducationalProgram.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Update(oEducationalProgram);
				await oCATSDatabaseContext.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!EducationalProgramExists(oEducationalProgram.EducationalProgramID))
				{ return NotFound(); }
				else
				{ throw; }
			}
			return RedirectToAction(nameof(Index));
		}
		#endregion
	}
}
