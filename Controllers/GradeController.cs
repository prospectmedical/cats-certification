﻿#region Init
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CATS.Data;
using CATS.Models;
#endregion

namespace CATS.Controllers
{
	public class GradeController : Controller
	{
		#region Declarations
		private readonly CATSDatabaseContext oCATSDatabaseContext;

		public GradeController(CATSDatabaseContext context)
		{ oCATSDatabaseContext = context; }
		#endregion

		#region Routines
		private bool GradeExists(int pGradeID)
		{ return oCATSDatabaseContext.Grade.Any(oGrade => oGrade.GradeID == pGradeID); }
		#endregion

		#region Index
		public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
		{
			ViewBag.CurrentSort = sortOrder;
			ViewBag.DescriptionSortParm = sortOrder == "Description" ? "Description desc" : "Description";

			if (searchString != null)
			{
				page = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["currentFilter"] = searchString;

			var query = from oGrade in oCATSDatabaseContext.Grade
									where oGrade.DeleteFlag == false && oGrade.CompanyID == Globals.CompanyID
									select oGrade;

			if (!String.IsNullOrEmpty(searchString))
			{
				query = query.Where(select => select.Description.Contains(searchString));
			}
			switch (sortOrder)
			{
				case "Description desc":
					query = query.OrderByDescending(select => select.Description);
					break;
				default:
					query = query.OrderBy(select => select.Description);
					break;
			}

			return View(await PaginatedList<Grade>.CreateAsync(query.AsNoTracking(), page ?? 1, Globals.PageSize));
		}
		#endregion

		#region CreateEdit
		public IActionResult CreateEdit()
		{
			var oGrade = new Grade
			{
				GradeID = 0,
				CompanyID = Globals.CompanyID,
				DeleteFlag = false
			};
			return View(oGrade);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit([Bind("GradeID, CompanyID, Description, DeleteFlag, RecordUpdate")] Grade oGrade)
		{
			if (ModelState.IsValid)
			{
				oGrade.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Add(oGrade);
				await oCATSDatabaseContext.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(oGrade);
		}

		[HttpGet("Grade/CreateEdit/{pGradeID:int}")]
		public async Task<IActionResult> CreateEdit(int? pGradeID)
		{
			if (pGradeID == null) { return NotFound(); }
			var oGrade = await oCATSDatabaseContext.Grade.SingleOrDefaultAsync(qGrade => qGrade.GradeID == pGradeID);
			if (oGrade == null) { return NotFound(); }
			return View(oGrade);
		}

		[HttpPost("Grade/CreateEdit/{pGradeID:int}")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateEdit(int pGradeID, [Bind("GradeID, CompanyID, Description, DeleteFlag, RecordUpdate")] Grade oGrade)
		{
			if (ModelState.IsValid)
			{
				try
				{
					oGrade.GradeID = pGradeID;
					oGrade.RecordUpdate = DateTime.Now;
					oCATSDatabaseContext.Update(oGrade);
					await oCATSDatabaseContext.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!GradeExists(oGrade.GradeID))
					{ return NotFound(); }
					else
					{ throw; }
				}
				return RedirectToAction(nameof(Index));
			}
			return View(oGrade);
		}
		#endregion

		#region Delete
		[HttpGet("Grade/Delete/{pGradeID:int}")]
		public async Task<IActionResult> Delete(int? pGradeID)
		{
			if (pGradeID == null) { return NotFound(); }
			var oGrade = await oCATSDatabaseContext.Grade.SingleOrDefaultAsync(qGrade => qGrade.GradeID == pGradeID);
			if (oGrade == null) { return NotFound(); }
			return View(oGrade);
		}

		[HttpPost("Grade/Delete/{pGradeID:int}"), ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int? pGradeID)
		{
			if (pGradeID == null) { return NotFound(); }
			var oGrade = await oCATSDatabaseContext.Grade.SingleOrDefaultAsync(qGrade => qGrade.GradeID == pGradeID);
			if (oGrade == null) { return NotFound(); }
			try
			{
				oGrade.DeleteFlag = true;
				oGrade.RecordUpdate = DateTime.Now;
				oCATSDatabaseContext.Update(oGrade);
				await oCATSDatabaseContext.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!GradeExists(oGrade.GradeID))
				{ return NotFound(); }
				else
				{ throw; }
			}
			return RedirectToAction(nameof(Index));
		}
		#endregion
	}
}
