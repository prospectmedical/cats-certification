﻿#region Init
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CATS.Data;
using CATS.Models;
using Microsoft.Reporting.WebForms;
using ReportViewerForMvc;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web;
using Microsoft.Extensions.PlatformAbstractions;
//using System.Web.Mvc;
#endregion

namespace CATS.Controllers
{
	public class ReportsController : Controller
	{
		#region Declarations
		private readonly CATSDatabaseContext oCATSDatabaseContext;

		public ReportsController(CATSDatabaseContext context)
		{ oCATSDatabaseContext = context; }
		#endregion

		public ActionResult Index()
		{
			return View();
		}

		private readonly ApplicationEnvironment oApplicationEnvironment;

		public ReportsController(ApplicationEnvironment env)
		{
			oApplicationEnvironment = env;
		}

		public ActionResult ReportEmployee()
		{
			ReportViewer reportViewer = new ReportViewer();
			reportViewer.ProcessingMode = ProcessingMode.Local;
			reportViewer.SizeToReportContent = true;
			//reportViewer.Width = Unit.Percentage(900);
			//reportViewer.Height = Unit.Percentage(900);

			//	var connectionString = ConfigurationManager.ConnectionStrings["DbEmployeeConnectionString"].ConnectionString;
			//SqlConnection conx = new SqlConnection(connectionString); SqlDataAdapter adp = new SqlDataAdapter("SELECT * FROM Employee_tbt", conx);
			//adp.Fill(ds, ds.Employee_tbt.TableName);
			//_env = env;
			//string contentRootPath = _env.ApplicationBasePath;
			//reportViewer.LocalReport.ReportPath = HttpContext Server.MapPath("~/App_Data");

			reportViewer.LocalReport.ReportPath = oApplicationEnvironment.ApplicationBasePath + @"CATSReports01\Reports\TuitionRequestListClosed.rdl";

			//reportViewer.LocalReport.DataSources.Add(new ReportDataSource("MyDataSet", ds.Tables[0]));

			ViewBag.ReportViewer = reportViewer;
			return View();
		}

	}
}
