﻿#region Init
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CATS.Data;
using CATS.Models;
using Microsoft.AspNetCore.Http;
#endregion

namespace CATS.Controllers
{
	public class HomeController : Controller
    {
		#region Declarations
		private CATSDatabaseContext oCATSDatabaseContext;

		public HomeController(
					[FromServices] CATSDatabaseContext pCATSDatabaseContext)
		{
			oCATSDatabaseContext = pCATSDatabaseContext;
		}
		#endregion

		#region Index
		public IActionResult Index()
        {
			var sUserID = User.Identity.Name;
						
			if (sUserID.Length > 0)
			{
					int iPosition = sUserID.IndexOf("\\");
					sUserID = (iPosition > -1) ? sUserID.Substring(iPosition + 1, sUserID.Length - iPosition - 1) : string.Empty;
			}
			Account oAccount = oCATSDatabaseContext.Account.SingleOrDefault(Account => Account.UserID == sUserID && Account.DeleteFlag == false);
			if (oAccount != null)
			{
				if (oAccount.AccountID > 0)
				{		
					//HttpContext.Session.SetString("UserFirstName", oAccount.FirstName);
					//ViewBag.UserFirstName = HttpContext.Session.GetString("UserFirstName");
					return View();
				}
			}
			return RedirectToAction("LoginError", "Home");
    }
		#endregion

		#region About
		public IActionResult About()
        {
            return View();
        }
		#endregion

		#region Contact
		public IActionResult Contact()
        {
            return View();
        }
		#endregion

		#region LoginError
		public IActionResult LoginError()
        {
            return View();
        }
		#endregion
	}
}
