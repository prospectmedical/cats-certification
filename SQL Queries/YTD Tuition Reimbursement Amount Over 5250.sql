SELECT	EmployeeNumber, 
		LastName, 
		FirstName, 
		SUM(ReimbursementAmount) AS ReimbursementAmount
FROM    TuitionRequest
WHERE   YEAR(DateSentToAP) = 2019 AND 
		SentToAP = 1 AND 
		EducationalProgramID IN (1, 2) AND 
		DeleteFlag = 0
GROUP 
BY		EmployeeNumber, 
		LastName, 
		FirstName
HAVING  SUM(ReimbursementAmount) > 5250
ORDER 
BY		LastName, 
		FirstName