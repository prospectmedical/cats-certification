#pragma checksum "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fd34cc7b93b6eb170b8f706509983882ed536a49"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_TuitionRequest_EmployeeDetails), @"mvc.1.0.view", @"/Views/TuitionRequest/EmployeeDetails.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/TuitionRequest/EmployeeDetails.cshtml", typeof(AspNetCore.Views_TuitionRequest_EmployeeDetails))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/_ViewImports.cshtml"
using CATS;

#line default
#line hidden
#line 2 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/_ViewImports.cshtml"
using CATS.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fd34cc7b93b6eb170b8f706509983882ed536a49", @"/Views/TuitionRequest/EmployeeDetails.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7d5bf76eb933c6b28293be002d60abbf1f9c034a", @"/Views/_ViewImports.cshtml")]
    public class Views_TuitionRequest_EmployeeDetails : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CATS.Models.Employee>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "TuitionRequest", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "CreateEdit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-route-pNewFlag", "0", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "EmployeeIndex", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(109, 228, true);
            WriteLiteral("\r\n<h2 class=\"offseth2\">Tuition Request Processing: Employee Details</h2>\r\n\r\n  <hr class=\"style03\" />\r\n  <div class=\"row\" style=\"margin-bottom:5px;\">\r\n    <div class=\"col-md-3\" style=\"text-align:right; font-weight:bold;\">\r\n      ");
            EndContext();
            BeginContext(338, 50, false);
#line 10 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayNameFor(Model => Model.EmployeeNumber));

#line default
#line hidden
            EndContext();
            BeginContext(388, 49, true);
            WriteLiteral(":\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      ");
            EndContext();
            BeginContext(438, 46, false);
#line 13 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayFor(Model => Model.EmployeeNumber));

#line default
#line hidden
            EndContext();
            BeginContext(484, 214, true);
            WriteLiteral("\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"row\" style=\"margin-bottom:5px;\">\r\n    <div class=\"col-md-3\" style=\"text-align:right; font-weight:bold;\">\r\n      Employee Name:\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      ");
            EndContext();
            BeginContext(699, 40, false);
#line 22 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayFor(model => model.LastName));

#line default
#line hidden
            EndContext();
            BeginContext(739, 2, true);
            WriteLiteral(", ");
            EndContext();
            BeginContext(742, 41, false);
#line 22 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
                                            Write(Html.DisplayFor(model => model.FirstName));

#line default
#line hidden
            EndContext();
            BeginContext(783, 26, true);
            WriteLiteral("\r\n    </div>\r\n  </div>\r\n\r\n");
            EndContext();
            BeginContext(1122, 128, true);
            WriteLiteral("\r\n  <div class=\"row\" style=\"margin-bottom:5px;\">\r\n    <div class=\"col-md-3\" style=\"text-align:right; font-weight:bold;\">\r\n      ");
            EndContext();
            BeginContext(1251, 46, false);
#line 37 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayNameFor(model => model.DateOfHire));

#line default
#line hidden
            EndContext();
            BeginContext(1297, 49, true);
            WriteLiteral(":\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      ");
            EndContext();
            BeginContext(1347, 42, false);
#line 40 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayFor(model => model.DateOfHire));

#line default
#line hidden
            EndContext();
            BeginContext(1389, 152, true);
            WriteLiteral("\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"row\" style=\"margin-bottom:5px;\">\r\n    <div class=\"col-md-3\" style=\"text-align:right; font-weight:bold;\">\r\n      ");
            EndContext();
            BeginContext(1542, 50, false);
#line 46 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayNameFor(model => model.EmployeeStatus));

#line default
#line hidden
            EndContext();
            BeginContext(1592, 49, true);
            WriteLiteral(":\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      ");
            EndContext();
            BeginContext(1642, 46, false);
#line 49 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayFor(model => model.EmployeeStatus));

#line default
#line hidden
            EndContext();
            BeginContext(1688, 1, true);
            WriteLiteral("-");
            EndContext();
            BeginContext(1690, 49, false);
#line 49 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
                                                 Write(Html.DisplayFor(model => model.StatusDescription));

#line default
#line hidden
            EndContext();
            BeginContext(1739, 152, true);
            WriteLiteral("\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"row\" style=\"margin-bottom:5px;\">\r\n    <div class=\"col-md-3\" style=\"text-align:right; font-weight:bold;\">\r\n      ");
            EndContext();
            BeginContext(1892, 58, false);
#line 55 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayNameFor(model => model.EmploymentCategoryCode));

#line default
#line hidden
            EndContext();
            BeginContext(1950, 1, true);
            WriteLiteral("-");
            EndContext();
            BeginContext(1952, 58, false);
#line 55 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
                                                             Write(Html.DisplayNameFor(model => model.EmploymentCategoryName));

#line default
#line hidden
            EndContext();
            BeginContext(2010, 49, true);
            WriteLiteral(":\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      ");
            EndContext();
            BeginContext(2060, 54, false);
#line 58 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayFor(model => model.EmploymentCategoryCode));

#line default
#line hidden
            EndContext();
            BeginContext(2114, 1, true);
            WriteLiteral("-");
            EndContext();
            BeginContext(2116, 54, false);
#line 58 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
                                                         Write(Html.DisplayFor(model => model.EmploymentCategoryName));

#line default
#line hidden
            EndContext();
            BeginContext(2170, 152, true);
            WriteLiteral("\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"row\" style=\"margin-bottom:5px;\">\r\n    <div class=\"col-md-3\" style=\"text-align:right; font-weight:bold;\">\r\n      ");
            EndContext();
            BeginContext(2323, 44, false);
#line 64 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayNameFor(model => model.Position));

#line default
#line hidden
            EndContext();
            BeginContext(2367, 49, true);
            WriteLiteral(":\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      ");
            EndContext();
            BeginContext(2417, 46, false);
#line 67 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayFor(model => model.PositionNumber));

#line default
#line hidden
            EndContext();
            BeginContext(2463, 1, true);
            WriteLiteral("-");
            EndContext();
            BeginContext(2465, 40, false);
#line 67 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
                                                 Write(Html.DisplayFor(model => model.Position));

#line default
#line hidden
            EndContext();
            BeginContext(2505, 152, true);
            WriteLiteral("\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"row\" style=\"margin-bottom:5px;\">\r\n    <div class=\"col-md-3\" style=\"text-align:right; font-weight:bold;\">\r\n      ");
            EndContext();
            BeginContext(2658, 43, false);
#line 73 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayNameFor(model => model.JobCode));

#line default
#line hidden
            EndContext();
            BeginContext(2701, 1, true);
            WriteLiteral("-");
            EndContext();
            BeginContext(2703, 43, false);
#line 73 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
                                              Write(Html.DisplayNameFor(model => model.JobName));

#line default
#line hidden
            EndContext();
            BeginContext(2746, 49, true);
            WriteLiteral(":\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      ");
            EndContext();
            BeginContext(2796, 39, false);
#line 76 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayFor(model => model.JobCode));

#line default
#line hidden
            EndContext();
            BeginContext(2835, 1, true);
            WriteLiteral("-");
            EndContext();
            BeginContext(2837, 39, false);
#line 76 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
                                          Write(Html.DisplayFor(model => model.JobName));

#line default
#line hidden
            EndContext();
            BeginContext(2876, 152, true);
            WriteLiteral("\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"row\" style=\"margin-bottom:5px;\">\r\n    <div class=\"col-md-3\" style=\"text-align:right; font-weight:bold;\">\r\n      ");
            EndContext();
            BeginContext(3029, 45, false);
#line 82 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayNameFor(model => model.UnionCode));

#line default
#line hidden
            EndContext();
            BeginContext(3074, 49, true);
            WriteLiteral(":\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      ");
            EndContext();
            BeginContext(3124, 41, false);
#line 85 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayFor(model => model.UnionCode));

#line default
#line hidden
            EndContext();
            BeginContext(3165, 154, true);
            WriteLiteral("\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <div class=\"row\" style=\"margin-bottom:5px;\">\r\n    <div class=\"col-md-3\" style=\"text-align:right; font-weight:bold;\">\r\n      ");
            EndContext();
            BeginContext(3320, 46, false);
#line 92 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayNameFor(model => model.CostCenter));

#line default
#line hidden
            EndContext();
            BeginContext(3366, 49, true);
            WriteLiteral(":\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      ");
            EndContext();
            BeginContext(3416, 42, false);
#line 95 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayFor(model => model.CostCenter));

#line default
#line hidden
            EndContext();
            BeginContext(3458, 152, true);
            WriteLiteral("\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"row\" style=\"margin-bottom:5px;\">\r\n    <div class=\"col-md-3\" style=\"text-align:right; font-weight:bold;\">\r\n      ");
            EndContext();
            BeginContext(3611, 42, false);
#line 101 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayNameFor(model => model.Entity));

#line default
#line hidden
            EndContext();
            BeginContext(3653, 49, true);
            WriteLiteral(":\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      ");
            EndContext();
            BeginContext(3703, 38, false);
#line 104 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayFor(model => model.Entity));

#line default
#line hidden
            EndContext();
            BeginContext(3741, 152, true);
            WriteLiteral("\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"row\" style=\"margin-bottom:5px;\">\r\n    <div class=\"col-md-3\" style=\"text-align:right; font-weight:bold;\">\r\n      ");
            EndContext();
            BeginContext(3894, 45, false);
#line 110 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayNameFor(model => model.HomePhone));

#line default
#line hidden
            EndContext();
            BeginContext(3939, 49, true);
            WriteLiteral(":\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      ");
            EndContext();
            BeginContext(3989, 41, false);
#line 113 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
 Write(Html.DisplayFor(model => model.HomePhone));

#line default
#line hidden
            EndContext();
            BeginContext(4030, 26, true);
            WriteLiteral("\r\n    </div>\r\n  </div>\r\n\r\n");
            EndContext();
            BeginContext(4351, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(4666, 68, true);
            WriteLiteral("\r\n  <hr class=\"style03\" />\r\n  <div class=\"clear01\" />\r\n  <div>\r\n    ");
            EndContext();
            BeginContext(4734, 134, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ea34fcb25da04f2490bf33ea5a92c26e", async() => {
                BeginContext(4858, 6, true);
                WriteLiteral("Select");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-pEmployeeID", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 138 "/Users/bao/Desktop/VSProjects/28 - CATS Certification and tuition system/CATS/Views/TuitionRequest/EmployeeDetails.cshtml"
                                                                          WriteLiteral(Model.EmployeeID);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["pEmployeeID"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-pEmployeeID", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["pEmployeeID"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-pNewFlag", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["pNewFlag"] = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4868, 8, true);
            WriteLiteral(" |\r\n    ");
            EndContext();
            BeginContext(4876, 78, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "27ef280dc8b34719b3526d6cfc7dc450", async() => {
                BeginContext(4938, 12, true);
                WriteLiteral("Back to List");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4954, 12, true);
            WriteLiteral("\r\n  </div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CATS.Models.Employee> Html { get; private set; }
    }
}
#pragma warning restore 1591
