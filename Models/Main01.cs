﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CATS.Models
{
	public class Denied
	{
		public int DeniedID { get; set; }
		public Int32 CompanyID { get; set; }
		[Display(Name = "Denied/Incomplete")]
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Boolean DeleteFlag { get; set; }
		public DateTime RecordUpdate { get; set; }
	}

	public class EducationalInstitution
	{
		public int EducationalInstitutionID { get; set; }
		public Int32 CompanyID { get; set; }
		[Display(Name = "Educational Institution")]
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Boolean DeleteFlag { get; set; }
		public DateTime RecordUpdate { get; set; }
	}

	public class Entity
	{
		public int EntityID { get; set; }
		public Int32 CompanyID { get; set; }
		[Display(Name = "Entity")]
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Boolean DeleteFlag { get; set; }
		public DateTime RecordUpdate { get; set; }
	}

	public class FinancialAidType
	{
		public int FinancialAidTypeID { get; set; }
		public Int32 CompanyID { get; set; }
		[Display(Name = "Financial Aid Type")]
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Boolean DeleteFlag { get; set; }
		public DateTime RecordUpdate { get; set; }
	}

	public class Grade
	{
		public int GradeID { get; set; }
		public Int32 CompanyID { get; set; }
		[Display(Name = "Grade")]
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Boolean DeleteFlag { get; set; }
		public DateTime RecordUpdate { get; set; }
	}

	public class Major
	{
		public int MajorID { get; set; }
		public Int32 CompanyID { get; set; }
		[Display(Name = "Major")]
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public Boolean DeleteFlag { get; set; }
		public DateTime RecordUpdate { get; set; }
	}

	public class PaymentOption
	{
		public int PaymentOptionID { get; set; }

		public Int32 CompanyID { get; set; }

		[Display(Name = "Payment Option")]
		[Required]
		[StringLength(100)]
		public string Description { get; set; }

		public Boolean DeleteFlag { get; set; }

		public DateTime RecordUpdate { get; set; }
	}

	public class Request
	{
		public int RequestID { get; set; }
		public Int32 CompanyID { get; set; }
		[Display(Name = "Comment")]
		[Required]
		[StringLength(2000)]
		public string Comment { get; set; }
		public Boolean DeleteFlag { get; set; }
		public DateTime RecordUpdate { get; set; }
	}
}