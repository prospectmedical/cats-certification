﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CATS.Models
{
	public class Account
	{
		public int AccountID { get; set; }

		public string LastName { get; set; }

		public string FirstName { get; set; }

		public string UserID { get; set; }

		public Boolean DeleteFlag { get; set; }
	}
}