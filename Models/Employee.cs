﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CATS.Models
{
	public class Employee
	{
		[Key]
		public int EmployeeID { get; set; }

		[Display(Name = "Person ID")]
		public string PersonID { get; set; }

		[Display(Name = "Employee Number")]
		public Int64 EmployeeNumber { get; set; }

		[Display(Name = "Last Name")]
		public string LastName { get; set; }

		[Display(Name = "First Name")]
		public string FirstName { get; set; }

		//[Display(Name = "Social Security Number")]
		//public string SocialSecurityNumber { get; set; }

		[Display(Name = "Date of Hire")]
		public string DateOfHire { get; set; }

		[Display(Name = "Status")]
		public string EmployeeStatus { get; set; }

		[Display(Name = "Status Description")]
		public string StatusDescription { get; set; }

		[Display(Name = "Category Code")]
		public string EmploymentCategoryCode { get; set; }

		[Display(Name = "Name")]
		public string EmploymentCategoryName { get; set; }

		[Display(Name = "Position")]
		public string PositionNumber { get; set; }

		[Display(Name = "Position")]
		public string Position { get; set; }

		[Display(Name = "Job Code")]
		public string JobCode { get; set; }

		[Display(Name = "Name")]
		public string JobName { get; set; }

		[Display(Name = "Union Code")]
		public string UnionCode { get; set; }

		[Display(Name = "Cost Center")]
		public string CostCenter { get; set; }

		[Display(Name = "Entity")]
		public string Entity { get; set; }

		[Display(Name = "Phone")]
		public string HomePhone { get; set; }

		[Display(Name = "Company ID")]
		public string CATSCompanyID { get; set; }

		[Display(Name = "Department Number")]
		public string CATSDepartmentNumber { get; set; }

		[Display(Name = "eMail Address")]
		public string WorkEMail { get; set; }

		//[Display(Name = "YTD Tuition")]
		//[DisplayFormat(DataFormatString = "{0:C}")]
		//public double YTDTuitionReceived { get; set; }

		//[Display(Name = "Max. Reimbursement")]
		//[DisplayFormat(DataFormatString = "{0:C}")]
		//public double MaximumReimbursement { get; set; }
	}
}