﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CATS.Models
{
	public class TuitionRequest
	{
		[Key]
		public int TuitionRequestID { get; set; }

		public Int32 CompanyID { get; set; }

		[Display(Name = "Request Number")]
		public Int32 RequestNumber { get; set; }

		[Display(Name = "Person ID")]
		public string PersonID { get; set; }

		[Display(Name = "Employee Number")]
		public Int64 EmployeeNumber { get; set; }

		[Display(Name = "Last Name")]
		public string LastName { get; set; }

		[Display(Name = "First Name")]
		public string FirstName { get; set; }

		//[Display(Name = "Social Secutiy Number")]
		//public string SocialSecurityNumber { get; set; }

		[Display(Name = "Date of Hire")]
		public string DateOfHire { get; set; }

		[Display(Name = "Status")]
		public string EmployeeStatus { get; set; }

		[Display(Name = "Status Description")]
		public string StatusDescription { get; set; }

		[Display(Name = "Category Code")]
		public string EmploymentCategoryCode { get; set; }

		[Display(Name = "Name")]
		public string EmploymentCategoryName { get; set; }

		[Display(Name = "Position")]
		public string PositionNumber { get; set; }

		[Display(Name = "Position")]
		public string Position { get; set; }

		[Display(Name = "Job Code")]
		public string JobCode { get; set; }

		[Display(Name = "Name")]
		public string JobName { get; set; }

		[Display(Name = "Union Code")]
		public string UnionCode { get; set; }

		[Display(Name = "Department")]
		public string CostCenter { get; set; }

		[Display(Name = "Entity")]
		public string Entity { get; set; }

		[Display(Name = "Phone")]
		public string HomePhone { get; set; }

		[Display(Name ="eMail")]
		[StringLength(100, ErrorMessage = "eMail must be no more than {1} characters.")]
		[EmailAddress(ErrorMessage = "Invalid eMail Address")]
		public string EMailAddress { get; set; }

		[Display(Name = "Date Entered")]
		public string DateEntered { get; set; }

		[Display(Name = "Institution")]
		public Int32 EducationalInstitutionID { get; set; }
		public IEnumerable<SelectListItem> EducationalInstutions { get; set; }

		[Display(Name = "Program")]
		[Required, Range(1, int.MaxValue, ErrorMessage = "Select Program")]
		public Int32 EducationalProgramID { get; set; }
		public IEnumerable<SelectListItem> EducationalPrograms { get; set; }

		[Display(Name = "Major")]
		[Required, Range(1, int.MaxValue, ErrorMessage = "Select Major")]
		public Int32 MajorID { get; set; }
		public IEnumerable<SelectListItem> Majors { get; set; }

		[Display(Name = "Course Start")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", NullDisplayText = "MM-DD-YYYY", ApplyFormatInEditMode = true)]
		public DateTime CourseStartDate { get; set; }
		[Required(ErrorMessage = "Course Start is required")]

		[Display(Name = "Course End")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", NullDisplayText = "MM-DD-YYYY", ApplyFormatInEditMode = true)]
		public DateTime CourseEndDate { get; set; }
		[Required(ErrorMessage = "Course End is required")]

		[Display(Name = "Payment Option")]
		public Int32 PaymentOptionID { get; set; }
		public IEnumerable<SelectListItem> PaymentOptions { get; set; }

		[Display(Name = "Course 1")]
		[Required(ErrorMessage = "Course Title 1 is required")]
		[StringLength(150, ErrorMessage = "Course Title 1 must be no more than {1} characters.")]
		public string CourseTitle1 { get; set; }

		[Display(Name = "Tuition")]
		[DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
		[Required(ErrorMessage = "Tuition 1 is required")]
		public double TuitionAmount1 { get; set; }

		[Display(Name = "Grade")]
		public Int32 Grade1ID { get; set; }

		[Display(Name = "Course 2")]
		[StringLength(150, ErrorMessage = "Course Title 2 must be no more than {1} characters.")]
		public string CourseTitle2 { get; set; }

		[Display(Name = "Tuition")]
		[DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
		public double TuitionAmount2 { get; set; }

		[Display(Name = "Grade")]
		public Int32 Grade2ID { get; set; }

		[Display(Name = "Course 3")]
		[StringLength(150, ErrorMessage = "Course Title 3 must be no more than {1} characters.")]
		public string CourseTitle3 { get; set; }

		[Display(Name = "Tuition")]
		[DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
		public double TuitionAmount3 { get; set; }

		[Display(Name = "Grade")]
		public Int32 Grade3ID { get; set; }

		[Display(Name = "Course 4")]
		[StringLength(150, ErrorMessage = "Course Title 4 must be no more than {1} characters.")]
		public string CourseTitle4 { get; set; }

		[Display(Name = "Tuition")]
		[DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
		public double TuitionAmount4 { get; set; }

		[Display(Name = "Grade")]
		public Int32 Grade4ID { get; set; }

		[Display(Name = "Financial Aid")]
		public Int32 FinancialAidTypeID { get; set; }
		public IEnumerable<SelectListItem> FinancialAidTypes { get; set; }

		[Display(Name = "Aid Amount")]
		[DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
		public double FinancialAidAmount { get; set; }

		[Display(Name = "Status")]
		public Int32 StatusID { get; set; }
		public IEnumerable<SelectListItem> Statuses { get; set; }

		[Display(Name = "Denied/Incomplete")]
		public Int32 DeniedID { get; set; }
		public IEnumerable<SelectListItem> Denieds { get; set; }

		[Display(Name = "Comment")]
		[StringLength(2000)]
		public string Comment { get; set; }

		[Display(Name = "In Process eMail")]
		public Boolean SendProcessingEmail { get; set; }

		[Display(Name = "In Process Sent")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", NullDisplayText = "", ApplyFormatInEditMode = true)]
		public DateTime? ProcessingEmailSentDateTime { get; set; }

		[Display(Name = "Denied eMail")]
		public Boolean SendDeniedEmail { get; set; }

		[Display(Name = "Denied Sent")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy hh:mm tt}", NullDisplayText = "", ApplyFormatInEditMode = true)]
		public DateTime? DeniedEmailSentDateTime { get; set; }

		[Display(Name = "Approved eMail")]
		public Boolean SendApprovedEmail { get; set; }

		[Display(Name = "Approved Sent")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy hh:mm tt}", NullDisplayText = "", ApplyFormatInEditMode = true)]
		public DateTime? ApprovedEmailSentDateTime { get; set; }

		[Display(Name = "Incomplete eMail")]
		public Boolean SendIncompleteEmail { get; set; }

		[Display(Name = "Incomplete Sent")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy hh:mm tt}", NullDisplayText = "", ApplyFormatInEditMode = true)]
		public DateTime? IncompleteEmailSentDateTime { get; set; }

		//[Display(Name = "Percent")]
		//public Int32 PercentID { get; set; }
		//public IEnumerable<SelectListItem> Percents { get; set; }

		//[Display(Name = "Sent To")]
		//public Int32 SentToID { get; set; }
		//public IEnumerable<SelectListItem> SentTos { get; set; }

		//[Display(Name = "Date Sent")]
		//[DataType(DataType.Date)]
		//[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", NullDisplayText = "MM-DD-YYYY", ApplyFormatInEditMode = true)]
		//public DateTime? DateSent { get; set; }

		//[Display(Name = "Date Received")]
		//[DataType(DataType.Date)]
		//[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", NullDisplayText = "MM-DD-YYYY", ApplyFormatInEditMode = true)]
		//public DateTime? DateReceived { get; set; }

		[Display(Name = "Check Number")]
		public string CheckNumber { get; set; }

		[Display(Name = "Check Mailed")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", NullDisplayText = "MM-DD-YYYY", ApplyFormatInEditMode = true)]
		public DateTime? CheckMailDate { get; set; }

		[Display(Name = "Check Picked Up")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", NullDisplayText = "MM-DD-YYYY", ApplyFormatInEditMode = true)]
		public DateTime? CheckPickUpDate { get; set; }

		[Display(Name = "Reimbursement Percent")]
		[DisplayFormat(DataFormatString = "{0:0%}", ApplyFormatInEditMode = true)]
		public double ReimbursementPercent { get; set; }

		[Display(Name = "Max. Reimbursement")]
		[DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
		public double MaximumReimbursement { get; set; }

		[Display(Name = "YTD Tuition")]
		[DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
		public double YTDTuitionReceived { get; set; }

		[Display(Name = "YTD Cert. Tuition")]
		[DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
		public double YTDCertificationTuitionReceived { get; set; }

		[Display(Name = "Tuition Award")]
		[DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
		public double ReimbursementAmount { get; set; }

		[Display(Name = "Account Number")]
		public string AccountNumber { get; set; }

		[Display(Name = "Closed")]
		public Boolean Closed { get; set; }

		public Boolean DeleteFlag { get; set; }

		public DateTime RecordUpdate { get; set; }
	}

	public class RequestNumberTracking
	{
		[Key]
		public int RequestNumberTrackingID { get; set; }

		public string TrackingGUID { get; set; }
	}

	public class TuitionRequestDisplay
	{
		[Key]
		public int TuitionRequestID { get; set; }

		public Int32 CompanyID { get; set; }

		[Display(Name = "Request Number")]
		public Int32 RequestNumber { get; set; }

		[Display(Name = "Employee Number")]
		public Int64 EmployeeNumber { get; set; }

		[Display(Name = "Last Name")]
		public string LastName { get; set; }

		[Display(Name = "First Name")]
		public string FirstName { get; set; }

		[Display(Name = "Status")]
		public string StatusDescription { get; set; }

		[Display(Name = "Closed")]
		public Boolean Closed { get; set; }
	}

	public class TuitionRequestClosed
	{
		[Key]
		public int TuitionRequestID { get; set; }

		public Int32 CompanyID { get; set; }

		[Display(Name = "Request Number")]
		public Int32 RequestNumber { get; set; }

		[Display(Name = "Employee Number")]
		public Int64 EmployeeNumber { get; set; }

		[Display(Name = "Last Name")]
		public string LastName { get; set; }

		[Display(Name = "First Name")]
		public string FirstName { get; set; }

		//[Display(Name = "Social Secutiy Number")]
		//public string SocialSecurityNumber { get; set; }

		[Display(Name = "Date of Hire")]
		public string DateOfHire { get; set; }

		[Display(Name = "Status")]
		public string EmployeeStatus { get; set; }

		[Display(Name = "Status Description")]
		public string StatusDescription { get; set; }

		[Display(Name = "Category Code")]
		public string EmploymentCategoryCode { get; set; }

		[Display(Name = "Name")]
		public string EmploymentCategoryName { get; set; }

		[Display(Name = "Position")]
		public string PositionNumber { get; set; }

		[Display(Name = "Position")]
		public string Position { get; set; }

		[Display(Name = "Job Code")]
		public string JobCode { get; set; }

		[Display(Name = "Name")]
		public string JobName { get; set; }

		[Display(Name = "Union Code")]
		public string UnionCode { get; set; }

		[Display(Name = "Department")]
		public string CostCenter { get; set; }

		[Display(Name = "Entity")]
		public string Entity { get; set; }

		[Display(Name = "Home Phone")]
		public string HomePhone { get; set; }

		[Display(Name = "eMail")]
		public string EMailAddress { get; set; }

		[Display(Name = "Date Entered")]
		public string DateEntered { get; set; }

		[Display(Name = "Institution")]
		public string EducationalInstitution { get; set; }

		public Int32 EducationalProgramID { get; set; }

		[Display(Name = "Program")]
		public string EducationalProgram { get; set; }

		[Display(Name = "Major")]
		public string Major { get; set; }

		[Display(Name = "Course Start")]
		//[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", NullDisplayText = "")]
		public DateTime CourseStartDate { get; set; }

		[Display(Name = "Course End")]
		//[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", NullDisplayText = "")]
		public DateTime CourseEndDate { get; set; }

		[Display(Name = "Payment Option")]
		public string PaymentOption { get; set; }

		[Display(Name = "Course 1")]
		public string CourseTitle1 { get; set; }

		[Display(Name = "Tuition")]
		[DisplayFormat(DataFormatString = "{0:F2}")]
		public double TuitionAmount1 { get; set; }

		[Display(Name = "Grade")]
		public string Grade1 { get; set; }

		[Display(Name = "Course 2")]
		public string CourseTitle2 { get; set; }

		[Display(Name = "Tuition")]
		[DisplayFormat(DataFormatString = "{0:F2}")]
		public double TuitionAmount2 { get; set; }

		[Display(Name = "Grade")]
		public string Grade2 { get; set; }

		[Display(Name = "Course 3")]
		public string CourseTitle3 { get; set; }

		[Display(Name = "Tuition")]
		[DisplayFormat(DataFormatString = "{0:F2}")]
		public double TuitionAmount3 { get; set; }

		[Display(Name = "Grade")]
		public string Grade3 { get; set; }

		[Display(Name = "Course 4")]
		public string CourseTitle4 { get; set; }

		[Display(Name = "Tuition")]
		[DisplayFormat(DataFormatString = "{0:F2}")]
		public double TuitionAmount4 { get; set; }

		[Display(Name = "Grade")]
		public string Grade4 { get; set; }

		[Display(Name = "Financial Aid")]
		public string FinancialAidType { get; set; }

		[Display(Name = "Aid Amount")]
		[DisplayFormat(DataFormatString = "{0:F2}")]
		public double FinancialAidAmount { get; set; }

		[Display(Name = "Status")]
		public string Status { get; set; }

		[Display(Name = "Denied/Incomplete")]
		public string Denied { get; set; }

		[Display(Name = "Comment")]
		public string Comment { get; set; }

		[Display(Name = "In Process eMail")]
		public Boolean SendProcessingEmail { get; set; }

		[Display(Name = "In Process Sent")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", NullDisplayText = "", ApplyFormatInEditMode = true)]
		public DateTime? ProcessingEmailSentDateTime { get; set; }

		[Display(Name = "Denied eMail")]
		public Boolean SendDeniedEmail { get; set; }

		[Display(Name = "Denied Sent")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy hh:mm tt}", NullDisplayText = "", ApplyFormatInEditMode = true)]
		public DateTime? DeniedEmailSentDateTime { get; set; }

		[Display(Name = "Approved eMail")]
		public Boolean SendApprovedEmail { get; set; }

		[Display(Name = "Approved Sent")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy hh:mm tt}", NullDisplayText = "", ApplyFormatInEditMode = true)]
		public DateTime? ApprovedEmailSentDateTime { get; set; }

		[Display(Name = "Incomplete eMail")]
		public Boolean SendIncompleteEmail { get; set; }

		[Display(Name = "Incomplete Sent")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy hh:mm tt}", NullDisplayText = "", ApplyFormatInEditMode = true)]
		public DateTime? IncompleteEmailSentDateTime { get; set; }

		[Display(Name = "Check Number")]
		public string CheckNumber { get; set; }

		[Display(Name = "Check Mailed")]
		//[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", NullDisplayText = "")]
		public DateTime? CheckMailDate { get; set; }

		[Display(Name = "Check Picked Up")]
		//[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", NullDisplayText = "")]
		public DateTime? CheckPickUpDate { get; set; }

		[Display(Name = "Reimbursement Percent")]
		[DisplayFormat(DataFormatString = "{0:0%}")]
		public double ReimbursementPercent { get; set; }

		[Display(Name = "Max. Reimbursement")]
		[DisplayFormat(DataFormatString = "{0:C}")]
		public double MaximumReimbursement { get; set; }

		[Display(Name = "YTD Tuition")]
		[DisplayFormat(DataFormatString = "{0:C}")]
		public double YTDTuitionReceived { get; set; }

		[Display(Name = "YTD Cert. Tuition")]
		[DisplayFormat(DataFormatString = "{0:C}")]
		public double YTDCertificationTuitionReceived { get; set; }

		[Display(Name = "Tuition Award")]
		[DisplayFormat(DataFormatString = "{0:C}")]
		public double ReimbursementAmount { get; set; }

		[Display(Name = "Account Number")]
		public string AccountNumber { get; set; }

		[Display(Name = "Closed")]
		public string Closed { get; set; }

		public Boolean DeleteFlag { get; set; }

		public DateTime RecordUpdate { get; set; }
	}
}