﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CATS.Models
{
	public class EducationalProgram
	{
		public int EducationalProgramID { get; set; }
		public Int32 CompanyID { get; set; }
		[Display(Name = "Educational Program")]
		[Required]
		[StringLength(100)]
		public string Description { get; set; }
		public int SortOrder { get; set; }
		public Boolean DeleteFlag { get; set; }
		public DateTime RecordUpdate { get; set; }
	}
}