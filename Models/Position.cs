﻿using System;

namespace CATS.Models
{
	public class Position
	{
		public int PositionID { get; set; }

		public string PositionNumber { get; set; }

		public string Title { get; set; }

		public Boolean DeleteFlag { get; set; }
	}
}