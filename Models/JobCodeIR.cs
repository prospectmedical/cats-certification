﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CATS.Models
{
	public class JobCodeIR
	{
		[Key]
		public int JobCodeIRID { get; set; }

		public string JobCode { get; set; }

		public Boolean DeleteFlag { get; set; }
	}
}