﻿using System.Web.Mvc;
using CATS.Models;
using Microsoft.EntityFrameworkCore;

namespace CATS.Data
{
	public class UnityInterfacesDatabaseContext : DbContext
	{
		public UnityInterfacesDatabaseContext(DbContextOptions<UnityInterfacesDatabaseContext> options)
				: base(options)
		{
		}
		public DbSet<Employee> Employee { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Employee>().ToTable("vCATSEmployeeList");
			modelBuilder.Ignore<SelectListItem>();
			modelBuilder.Ignore<SelectListGroup>();
		}
	}
}