﻿using System.Web.Mvc;
using CATS.Models;
using Microsoft.EntityFrameworkCore;

namespace CATS.Data
{
	public class CATSDatabaseContext : DbContext
	{
		public CATSDatabaseContext(DbContextOptions<CATSDatabaseContext> options)
				: base(options)
		{
		}
		public DbSet<Denied> Denied { get; set; }
		public DbSet<EducationalInstitution> EducationalInstitution { get; set; }
		public DbSet<EducationalProgram> EducationalProgram { get; set; }
		public DbSet<Entity> Entity { get; set; }
		public DbSet<FinancialAidType> FinancialAidType { get; set; }
		public DbSet<Grade> Grade { get; set; }
		public DbSet<Major> Major { get; set; }
		public DbSet<PaymentOption> PaymentOption { get; set; }
		public DbSet<RequestNumberTracking> RequestNumberTracking { get; set; }
		public DbSet<Status> Status { get; set; }
		public DbSet<TuitionRequest> TuitionRequests { get; set; }
		public DbSet<TuitionRequestClosed> TuitionRequestsClosed { get; set; }
		public DbSet<Account> Account { get; set; }
		public DbSet<TuitionRequestDisplay> TuitionRequestDisplay { get; set; }
		//public DbSet<Position> Position { get; set; }
		public DbSet<JobCodeIR> JobCodeIR { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Denied>().ToTable("Denied");
			modelBuilder.Entity<EducationalInstitution>().ToTable("EducationalInstitution");
			modelBuilder.Entity<EducationalProgram>().ToTable("EducationalProgram");
			modelBuilder.Entity<Entity>().ToTable("Entity");
			modelBuilder.Entity<FinancialAidType>().ToTable("FinancialAidType");
			modelBuilder.Entity<Grade>().ToTable("Grade");
			modelBuilder.Entity<Major>().ToTable("Major");
			modelBuilder.Entity<PaymentOption>().ToTable("PaymentOption");
			modelBuilder.Entity<RequestNumberTracking>().ToTable("RequestNumberTracking");
			modelBuilder.Entity<Status>().ToTable("Status");
			modelBuilder.Entity<TuitionRequest>().ToTable("TuitionRequest");
			modelBuilder.Entity<TuitionRequestClosed>().ToTable("vTuitionRequestClosed");
			modelBuilder.Entity<Account>().ToTable("Account");
			modelBuilder.Entity<TuitionRequestDisplay>().ToTable("vTuitionRequestDisplay01");
			//modelBuilder.Entity<Position>().ToTable("Position");
			modelBuilder.Entity<JobCodeIR>().ToTable("JobCodeIR");

			modelBuilder.Ignore<SelectListItem>();
			modelBuilder.Ignore<SelectListGroup>();
		}
	}
}