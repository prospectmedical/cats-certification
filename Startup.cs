﻿#region Init
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using CATS.Data;	
#endregion

namespace CATS
{
	#region Startup
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{ Configuration = configuration; }

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc();
			services.AddDbContext<CATSDatabaseContext>(options => options.UseSqlServer(Configuration.GetConnectionString("CATSDatabaseConnection")));
			services.AddDbContext<OracleHRDatabaseContext>(options => options.UseSqlServer(Configuration.GetConnectionString("OracleHRDatabaseConnection"), opt => opt.UseRowNumberForPaging()));
			//services.AddDbContext<UnityInterfacesDatabaseContext>(options => options.UseSqlServer(Configuration.GetConnectionString("UnityInterfacesDatabaseConnection"), opt => opt.UseRowNumberForPaging()));
	


			//services.AddDistributedMemoryCache();
			//services.AddSession(options => {
			//	options.IdleTimeout = TimeSpan.FromMinutes(30);
			//	options.CookieName = ".CATS";
			//});
		}
		#endregion

		#region Configure
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseBrowserLink();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
			}
			app.UseStaticFiles();
			app.UseMvc(routes =>
			{
				routes.MapRoute(name: "default", template: "{controller=Home}/{action=Index}/{id?}");
			});
			//app.UseSession();
			Globals.CompanyID = 1;
			Globals.PageSize = 15;
			
		}
		#endregion
	}
}