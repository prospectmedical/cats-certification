﻿#region Init
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
#endregion

namespace CATS
{
	public class Program
	{
		#region Main
		public static void Main(string[] args)
		{
			var host = BuildWebHost(args);

			using (var scope = host.Services.CreateScope())
			{
				var services = scope.ServiceProvider;
			}

			host.Run();
		}
		#endregion

		#region BuildWebHost
		public static IWebHost BuildWebHost(string[] args) =>
						WebHost.CreateDefaultBuilder(args)
								.UseStartup<Startup>()
								.Build();
	}
	#endregion
}