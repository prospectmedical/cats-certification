UPDATE	TuitionRequest
SET		Entity = (SELECT TOP 1 LTRIM(RTRIM(ReportingCompany)) FROM OracleHR.dbo.Assignment 
					WHERE OracleHR.dbo.Assignment.PersonID = TuitionRequest.PersonID AND Assignment.PrimaryFlag = 'y' AND Assignment.DeleteFlag = 0
                    ORDER BY Assignment.RecordUpdate DESC),
		PositionNumber = (SELECT TOP 1 LTRIM(RTRIM(PositionNumber)) FROM OracleHR.dbo.Assignment 
					WHERE OracleHR.dbo.Assignment.PersonID = TuitionRequest.PersonID AND Assignment.PrimaryFlag = 'y' AND Assignment.DeleteFlag = 0
                    ORDER BY Assignment.RecordUpdate DESC),
		EmploymentCategoryCode = (SELECT TOP 1 LTRIM(RTRIM(EmploymentCategoryCode)) FROM OracleHR.dbo.Assignment 
					WHERE OracleHR.dbo.Assignment.PersonID = TuitionRequest.PersonID AND Assignment.PrimaryFlag = 'y' AND Assignment.DeleteFlag = 0
                    ORDER BY Assignment.RecordUpdate DESC),
		EmploymentCategoryName = (SELECT TOP 1 LTRIM(RTRIM(EmploymentCategoryName)) FROM OracleHR.dbo.Assignment 
					WHERE OracleHR.dbo.Assignment.PersonID = TuitionRequest.PersonID AND Assignment.PrimaryFlag = 'y' AND Assignment.DeleteFlag = 0
                    ORDER BY Assignment.RecordUpdate DESC),		
		Position = (SELECT TOP 1 LTRIM(RTRIM(PositionName)) FROM OracleHR.dbo.Assignment 
					WHERE OracleHR.dbo.Assignment.PersonID = TuitionRequest.PersonID AND Assignment.PrimaryFlag = 'y' AND Assignment.DeleteFlag = 0
                    ORDER BY Assignment.RecordUpdate DESC),
		JobCode = (SELECT TOP 1 LTRIM(RTRIM(JobCode)) FROM OracleHR.dbo.Assignment 
					WHERE OracleHR.dbo.Assignment.PersonID = TuitionRequest.PersonID AND Assignment.PrimaryFlag = 'y' AND Assignment.DeleteFlag = 0
                    ORDER BY Assignment.RecordUpdate DESC),
		JobName = (SELECT TOP 1 LTRIM(RTRIM(JobName)) FROM OracleHR.dbo.Assignment 
					WHERE OracleHR.dbo.Assignment.PersonID = TuitionRequest.PersonID AND Assignment.PrimaryFlag = 'y' AND Assignment.DeleteFlag = 0
                    ORDER BY Assignment.RecordUpdate DESC),
		EmployeeStatus = (SELECT TOP 1 LTRIM(RTRIM(AssignmentStatusCode)) FROM OracleHR.dbo.Assignment 
					WHERE OracleHR.dbo.Assignment.PersonID = TuitionRequest.PersonID AND Assignment.PrimaryFlag = 'y' AND Assignment.DeleteFlag = 0
                    ORDER BY Assignment.RecordUpdate DESC),
		StatusDescription = (SELECT TOP 1 LTRIM(RTRIM(AssignmentStatusName)) FROM OracleHR.dbo.Assignment 
					WHERE OracleHR.dbo.Assignment.PersonID = TuitionRequest.PersonID AND Assignment.PrimaryFlag = 'y' AND Assignment.DeleteFlag = 0
                    ORDER BY Assignment.RecordUpdate DESC),
		UnionCode = (SELECT TOP 1 LTRIM(RTRIM(BargainingUnitCode)) FROM OracleHR.dbo.Assignment 
					WHERE OracleHR.dbo.Assignment.PersonID = TuitionRequest.PersonID AND Assignment.PrimaryFlag = 'y' AND Assignment.DeleteFlag = 0
                    ORDER BY Assignment.RecordUpdate DESC),
		CostCenter = (SELECT TOP 1 LTRIM(RTRIM(CATSDepartmentNumber)) FROM OracleHR.dbo.Assignment 
					WHERE OracleHR.dbo.Assignment.PersonID = TuitionRequest.PersonID AND Assignment.PrimaryFlag = 'y' AND Assignment.DeleteFlag = 0
                    ORDER BY Assignment.RecordUpdate DESC)
WHERE	PersonID IS NOT NULL AND
		ISDATE(DateSentToAP) = 0 AND
		Closed = 0 AND
		Converted = 0 AND
		DeleteFlag = 0

UPDATE	TuitionRequest
SET		AccountNumber = 
		CASE (SELECT TOP 1 LTRIM(RTRIM(CATSCompanyID)) FROM OracleHR.dbo.Assignment 
					WHERE OracleHR.dbo.Assignment.PersonID = TuitionRequest.PersonID AND Assignment.PrimaryFlag = 'y' AND Assignment.DeleteFlag = 0
                    ORDER BY Assignment.RecordUpdate DESC)
			WHEN '4110' THEN '4110'
			WHEN '9040' THEN '9040'
			WHEN '4400' THEN '4400' + ' ' + CostCenter + ' 79110'
			ELSE (SELECT TOP 1 LTRIM(RTRIM(CATSCompanyID)) FROM OracleHR.dbo.Assignment 
					WHERE OracleHR.dbo.Assignment.PersonID = TuitionRequest.PersonID AND Assignment.PrimaryFlag = 'y' AND Assignment.DeleteFlag = 0
                    ORDER BY Assignment.RecordUpdate DESC) + ' ' + CostCenter + ' 62070'
		END
WHERE	PersonID IS NOT NULL AND
		ISDATE(DateSentToAP) = 0 AND
		Closed = 0 AND
		Converted = 0 AND
		DeleteFlag = 0

UPDATE	TuitionRequest
SET		Converted = 1
WHERE	PersonID IS NOT NULL AND
		ISDATE(DateSentToAP) = 0 AND
		Closed = 0 AND
		Converted = 0 AND
		DeleteFlag = 0