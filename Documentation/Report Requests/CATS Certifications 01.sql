SELECT	TuitionRequest.RequestNumber, 
		TuitionRequest.EmployeeNumber, 
		TuitionRequest.LastName, 
		TuitionRequest.FirstName, 
		--TuitionRequest.EmployeeStatus, 
        TuitionRequest.StatusDescription, 
		--TuitionRequest.PositionNumber, 
		TuitionRequest.Position, 
		CONVERT(varchar, TuitionRequest.CourseStartDate, 110) AS CourseStartDate, 
		CONVERT(varchar, TuitionRequest.CourseEndDate, 110) AS CourseEndDate,
		PaymentOption.[Description] AS PayemntOption, 
		EducationalProgram.[Description] AS EducationalProgram, 
		TuitionRequest.CourseTitle1, 
		TuitionRequest.TuitionAmount1, 
		Grade_1.[Description] AS Grdae1, 
		TuitionRequest.CourseTitle2, 
		TuitionRequest.TuitionAmount2, 
		Grade_2.[Description] AS Grade2, 
		TuitionRequest.CourseTitle3, 
		TuitionRequest.TuitionAmount3, 
		Grade_3.[Description] AS Grade3, 
		TuitionRequest.CourseTitle4, 
		TuitionRequest.TuitionAmount4, 
		Grade_4.[Description] AS Grade4, 
		FORMAT(TuitionRequest.ReimbursementAmount, 'C') AS ReimbursementAmount, 
		CASE TuitionRequest.Closed WHEN 1 Then 'Yes' ELSE 'No' END AS Closed
FROM    TuitionRequest 
		LEFT OUTER JOIN EducationalProgram ON TuitionRequest.EducationalProgramID = EducationalProgram.EducationalProgramID 
		LEFT OUTER JOIN Grade AS Grade_1 ON TuitionRequest.Grade1ID = Grade_1.GradeID 
		LEFT OUTER JOIN Grade AS Grade_2 ON TuitionRequest.Grade2ID = Grade_2.GradeID 
		LEFT OUTER JOIN Grade AS Grade_3 ON TuitionRequest.Grade3ID = Grade_3.GradeID 
		LEFT OUTER JOIN Grade AS Grade_4 ON TuitionRequest.Grade4ID = Grade_4.GradeID  
		LEFT OUTER JOIN PaymentOption ON TuitionRequest.PaymentOptionID = PaymentOption.PaymentOptionID
WHERE   EducationalProgram.EducationalProgramID = 3 AND 
		TuitionRequest.DeleteFlag = 0
ORDER
BY		TuitionRequest.CourseStartDate,
		TuitionRequest.LastName, 
		TuitionRequest.FirstName