SELECT        RequestNumber, EmployeeNumber, LastName, FirstName, PositionNumber, Position, YTDTuitionReceived
FROM            dbo.TuitionRequest
WHERE        (MaximumReimbursement = 8000) AND (YEAR(CourseStartDate) = 2019)
order by lastname, firstname, RequestNumber